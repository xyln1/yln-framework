package com.yln.eventbus.core;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Maps;
import com.google.common.eventbus.EventBus;

import java.util.Iterator;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @description EventBus注册
 * @author: xiaoyulin
 * @create: 2021-05-18 10:40
 **/
public class EventBusRegistry {

    private final static ConcurrentMap<String, CopyOnWriteArraySet<EventBus>> eventBusesMap = Maps.newConcurrentMap();

    /**
     * 注册EventBus
     * @param eventType
     * @param eventBus
     */
    public static void register(String eventType, EventBus eventBus){
        CopyOnWriteArraySet<EventBus> eventBusSet = eventBusesMap.get(eventType);
        if(eventBusSet == null){
            CopyOnWriteArraySet<EventBus> newSet = new CopyOnWriteArraySet();
            eventBusSet = MoreObjects.firstNonNull(eventBusesMap.putIfAbsent(eventType, newSet), newSet);
        }
        eventBusSet.add(eventBus);
    }

    /**
     * 通过事件类型获取eventbus
     * @param eventType
     * @return
     */
    public static Iterator<EventBus> getEventBuses(String eventType){
        CopyOnWriteArraySet<EventBus> eventBusSet = eventBusesMap.get(eventType);
        if(eventBusSet != null){
            return eventBusSet.iterator();
        }
        return null;
    }
}
