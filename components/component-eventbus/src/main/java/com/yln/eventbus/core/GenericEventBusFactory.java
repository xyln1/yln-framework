package com.yln.eventbus.core;

import com.yln.common.dto.message.GenericEvent;
import com.yln.eventbus.exception.EventException;

/**
 * @description 通用事件总线工厂
 * @author: xiaoyulin
 * @create: 2021-05-26 15:12
 **/
public class GenericEventBusFactory extends AbstractEventBusFactory {

    @Override
    protected String getEventType(Object eventDto) {
        if(eventDto instanceof GenericEvent) {
            GenericEvent event = (GenericEvent) eventDto;
            return event.getEventType();
        }
        throw new EventException("Type must be GenericEvent.");
    }
}
