package com.yln.eventbus.deliver;

/**
 * @description 消息递传接口
 * @author: xiaoyulin
 * @create: 2021-05-18 13:03
 **/
public interface MessageDeliver {

    void delive(String payload);
}
