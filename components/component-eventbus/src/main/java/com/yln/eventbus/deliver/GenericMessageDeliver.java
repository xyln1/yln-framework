package com.yln.eventbus.deliver;

import com.yln.eventbus.core.GenericEventBusFactory;
import com.yln.eventbus.parser.GenericPayloadParser;

/**
 * @author xiaoyulin
 * @description 通用消息递传(消息体为:GenericEvent)
 * @date 2021-05-15
 */
public class GenericMessageDeliver extends BaseMessageDeliver {

    public GenericMessageDeliver() {
        super(new GenericPayloadParser(), new GenericEventBusFactory());
    }

}
