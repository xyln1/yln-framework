package com.yln.eventbus.core;

import com.google.common.eventbus.EventBus;

/**
 * @description 抽象事件总线
 * @author: xiaoyulin
 * @create: 2021-05-17 15:39
 **/
public abstract class AbstractEventBus extends EventBus implements EventBean {

    public AbstractEventBus(){
        // 注册事件处理器
        EventBusRegistry.register(this.getEventType(), this);
    }

    /**
     * 事件类型
     * @return
     */
    public abstract String getEventType();

}
