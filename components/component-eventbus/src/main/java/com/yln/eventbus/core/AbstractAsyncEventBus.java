package com.yln.eventbus.core;

import com.google.common.eventbus.AsyncEventBus;

import java.util.concurrent.Executors;

/**
 * @description 抽象异步事件总线
 * @author: xiaoyulin
 * @create: 2021-05-27 09:39
 **/
public abstract class AbstractAsyncEventBus extends AsyncEventBus implements EventBean {

    public AbstractAsyncEventBus(){
        super(Executors.newCachedThreadPool());
        // 注册事件处理器
        EventBusRegistry.register(this.getEventType(), this);
    }

    /**
     * 事件类型
     * @return
     */
    public abstract String getEventType();

}
