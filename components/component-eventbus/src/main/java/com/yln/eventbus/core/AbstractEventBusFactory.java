package com.yln.eventbus.core;

import com.google.common.eventbus.EventBus;
import com.yln.eventbus.exception.EventException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.util.Iterator;

public abstract class AbstractEventBusFactory implements EventBusFactory{

    private final static Logger log = LoggerFactory.getLogger(AbstractEventBusFactory.class);

    @Override
    public Iterator<EventBus> getEventBuses(String eventType) {
        if(StringUtils.isEmpty(eventType)){
            return null;
        }
        return EventBusRegistry.getEventBuses(eventType);
    }

    /**
     * 事件post
     * @param eventDto
     */
    @Override
    public void post(Object eventDto){
        log.info("eventDto dto:{}", eventDto);
        // 推送到相应事件的Subscriber
        Iterator<EventBus> eventBuses = this.getEventBuses(getEventType(eventDto));
        if(eventBuses != null){
            eventBuses.forEachRemaining(eventBus -> {
                try {
                    eventBus.post(eventDto);
                }catch (Exception e){
                    log.error("process exception:{}", e);
                }

            });
        }else{
            throw new EventException("No have matched EventBus.");
        }
    }

    /**
     * 获取事件类型
     * @param eventDto
     * @return
     */
    protected abstract String getEventType(Object eventDto);
}
