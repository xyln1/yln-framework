package com.yln.eventbus.annotation;


import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 事件
 *
 * @author xiaoyulin
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface Event {

    /**
     * 事件类型
     *
     * @return
     */
    String[] type() default {};

}
