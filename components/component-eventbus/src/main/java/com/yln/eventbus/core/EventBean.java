package com.yln.eventbus.core;

import org.springframework.beans.factory.InitializingBean;

/**
 * @description 事件Bean
 * @author: xiaoyulin
 * @create: 2021-05-17 15:39
 **/
public interface EventBean extends InitializingBean {

    /**
     * 事件类型
     * @return
     */
    public abstract String getEventType();
}
