package com.yln.eventbus.core;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;
import com.yln.common.dto.message.GenericEvent;

/**
 * @description
 * @author: xiaoyulin
 * @create: 2021-05-17 20:04
 **/
public interface GenericSubscriber {
    /**
     * 事件处理
     * @param genericEvent
     */
    @AllowConcurrentEvents
    @Subscribe
    void onEvent(GenericEvent genericEvent);
}
