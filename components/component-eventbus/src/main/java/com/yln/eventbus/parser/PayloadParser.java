package com.yln.eventbus.parser;

/**
 * @author xiaoyulin
 * @description 消息解析
 * @date 2021-05-15
 */
public interface PayloadParser {

    /**
     * 解析成对象
     * @param payload
     * @return
     */
    Object parse(String payload);
}
