package com.yln.eventbus.core;

import com.google.common.eventbus.EventBus;

import java.util.Iterator;
import java.util.List;

/**
 * @description EventBus工厂
 * @author: xiaoyulin
 * @create: 2021-05-17 19:33
 **/
public interface EventBusFactory {

    /**
     * 获取EventBus
     * @param eventType
     * @return
     */
    Iterator<EventBus> getEventBuses(String eventType);

    /**
     * post事件
     * @param eventDto
     */
    void post(Object eventDto);
}
