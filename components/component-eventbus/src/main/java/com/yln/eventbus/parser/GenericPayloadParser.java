package com.yln.eventbus.parser;

import com.alibaba.fastjson.JSONObject;
import com.yln.common.dto.message.GenericEvent;

/**
 * @author xiaoyulin
 * @description 通用消息体解析
 * @date 2021-05-15
 */
public class GenericPayloadParser implements PayloadParser {

    @Override
    public GenericEvent parse(String payload) {
        GenericEvent genericEvent = JSONObject.parseObject(payload, GenericEvent.class);
        return genericEvent;
    }
}
