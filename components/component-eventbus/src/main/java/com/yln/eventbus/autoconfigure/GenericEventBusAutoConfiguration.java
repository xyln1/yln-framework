package com.yln.eventbus.autoconfigure;

import com.yln.eventbus.deliver.GenericMessageDeliver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description 通用事件总线配置
 * @author: xiaoyulin
 * @create: 2021-05-17 20:21
 **/
@Configuration
public class GenericEventBusAutoConfiguration {

    @Bean
    public GenericMessageDeliver genericMessageDeliver(){
        GenericMessageDeliver messageDeliver = new GenericMessageDeliver();
        return messageDeliver;
    }
}
