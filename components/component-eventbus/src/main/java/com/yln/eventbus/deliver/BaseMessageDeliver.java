package com.yln.eventbus.deliver;

import com.yln.eventbus.core.EventBusFactory;
import com.yln.eventbus.exception.EventException;
import com.yln.eventbus.parser.PayloadParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @description 消息递传
 * @author: xiaoyulin
 * @create: 2021-05-17 17:54
 **/
public class BaseMessageDeliver implements MessageDeliver {

    private final static Logger log = LoggerFactory.getLogger(BaseMessageDeliver.class);

    protected PayloadParser payloadParser;

    protected EventBusFactory eventBusFactory;

    public BaseMessageDeliver(PayloadParser payloadParser, EventBusFactory eventBusFactory){
        this.payloadParser = payloadParser;
        this.eventBusFactory = eventBusFactory;
    }

    @Override
    public void delive(String payload) throws EventException {
        log.info("delive payload:{}", payload);
        // 解析payload成Java对象
        Object payloadDto = payloadParser.parse(payload);
        // 处理消息事件
        eventBusFactory.post(payloadDto);
    }

}
