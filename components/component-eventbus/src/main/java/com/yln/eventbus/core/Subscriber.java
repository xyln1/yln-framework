package com.yln.eventbus.core;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;
import com.yln.common.dto.message.Event;

public interface Subscriber {

    /**
     * 事件处理
     * @param event
     */
    @AllowConcurrentEvents
    @Subscribe
    void onEvent(Event event);
}
