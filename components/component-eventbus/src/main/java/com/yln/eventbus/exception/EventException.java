package com.yln.eventbus.exception;

/**
 * @description 事件异常
 * @author: xiaoyulin
 * @create: 2021-05-01 18:11
 **/
public class EventException extends RuntimeException{

    public EventException(String message) {
        super(message);
    }
}
