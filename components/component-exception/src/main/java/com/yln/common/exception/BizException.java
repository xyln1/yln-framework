package com.yln.common.exception;

/**
 * @description 业务异常
 * @author: xiaoyulin
 * @create: 2021-04-30 19:31
 **/
public class BizException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    protected String errorCode;
    protected Object[] args;

    public BizException() {
        super();
    }

    public BizException(String message, Throwable cause) {
        super(message, cause);
    }

    public BizException(String message) {
        super(message);
    }

    public BizException(String message, Object[] args, Throwable cause) {
        super(message, cause);
        this.args = args;
    }

    public BizException(String message, Object[] args) {
        super(message);
        this.args = args;
    }

    public BizException(String errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public BizException(String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public BizException(String errorCode, String message, Object[] args, Throwable cause) {
        this(errorCode, message, cause);
        this.args = args;
    }

    public BizException(String errorCode, String message, Object[] args) {
        this(errorCode, message);
        this.args = args;
    }

    public BizException(CodeMsg codeMsg) {

        super(codeMsg.getErrMsg());
        this.errorCode = codeMsg.getErrCode();
    }
    public BizException(Throwable cause) {
        super(cause);
    }

    public Object[] getArgs() {
        return args;
    }

    public String getErrorCode() {
        return errorCode;
    }

    @Override
    public String toString() {
        return "BizException{" +
                "errorCode=" + errorCode +
                ", message=" + super.getMessage() +
                '}';
    }
}
