package com.yln.common.exception;

/**
 * @description 未经许可异常（401 Unauthorized）
 * @author: xiaoyulin
 * @create: 2021-05-01 18:07
 **/
public class UnauthorizedException extends RuntimeException{

    public UnauthorizedException(String message) {
        super(message);
    }
}
