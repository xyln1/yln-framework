package com.yln.common.exception;

/**
 * @description 禁止访问异常（403 Forbidden）
 * @author: xiaoyulin
 * @create: 2021-05-01 18:11
 **/
public class ForbiddenException extends RuntimeException{

    public ForbiddenException(String message) {
        super(message);
    }
}
