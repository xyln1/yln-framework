package com.yln.common.exception;

/**
 * @description 错误码接口
 * @author: xiaoyulin
 * @create: 2021-04-30 19:34
 **/
public interface CodeMsg {

    String getErrCode();

    String getErrMsg();
}
