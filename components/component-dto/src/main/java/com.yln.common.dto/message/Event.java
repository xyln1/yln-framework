package com.yln.common.dto.message;

import java.io.Serializable;

/**
 * @description 事件
 * @author: xiaoyulin
 * @create: 2021-05-15 11:19
 **/
public abstract class Event implements Serializable {

    private static final long serialVersionUID = 1L;

}
