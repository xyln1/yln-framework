package com.yln.common.dto.message;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 通用事件（消息体）
 * @author: xiaoyulin
 * @create: 2021-05-15 11:20
 **/
public class GenericEvent extends Event implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 对象id
     */
	private Long objectId;
    /**
     * 对象类型
     */
	private String objectType;
    /**
     * 事件类型
     */
	private String eventType;
    /**
     * 其它参数
     */
    private Map<String, Object> attributes;
    /**
     * 无特定，可以灵活跟随信息
     */
	private String addition;
    /**
     * 系统类型
     */
	private String systemType;
    /**
     * 链路追踪id
     */
	private String traceId;
    /**
     * 来源（生产消息应用）
     */
	private String from;

    /**
     * 消息创建时间（毫秒数）
     */
	private Long createMilTime;
	private String operatorName;

    public GenericEvent(){
        super();
        attributes = new HashMap<String,Object>();
    }
	
	public GenericEvent(Long objectId, String objectType, String eventType) {
		this.objectId = objectId;
		this.objectType = objectType;
		this.eventType = eventType;
		this.attributes = new HashMap<String,Object>();
	}

    public GenericEvent(Long objectId, String objectType, String eventType, Map<String, Object> attributes) {
        this.objectId = objectId;
        this.objectType = objectType;
        this.eventType = eventType;
        this.attributes = attributes;
    }

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getAddition() {
		return addition;
	}

	public void setAddition(String addition) {
		this.addition = addition;
	}

	public String getSystemType() {
		return systemType;
	}

	public void setSystemType(String systemType) {
		this.systemType = systemType;
	}

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

	public void putAttribute(String key, Object value) {
		attributes.put(key, value);
	}

	public Long getCreateMilTime() {
		return createMilTime;
	}

	public void setCreateMilTime(Long createMilTime) {
		this.createMilTime = createMilTime;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	@Override
	public String toString() {
		return "GenericEvent{" +
				"objectId=" + objectId +
				", objectType='" + objectType + '\'' +
				", eventType='" + eventType + '\'' +
				", attributes=" + attributes +
				", addition='" + addition + '\'' +
				", systemType='" + systemType + '\'' +
				", traceId='" + traceId + '\'' +
				", from='" + from + '\'' +
				", createMilTime=" + createMilTime +
				", operatorName='" + operatorName + '\'' +
				'}';
	}
}
