package com.yln.common.dto;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @description 响应结果
 * @author: xiaoyulin
 * @create: 2021-05-02 16:09
 **/
public class Response<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    public final static String FAIL_CODE = "500";
    public final static String FAIL_MSG = "Internal Server Error";

    private boolean success = true;


    private T data;
    /**
     * 错误码
     */
    private String errCode;

    private String errMsg;

    public Response(){
    }

    public Response(String errCode, String errMsg, T data) {
        this.errCode = errCode;
        this.errMsg = errMsg;
        this.data = data;
    }
    public Response(String errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
        this.data = (T) new HashMap(1);
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
        this.success = false;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
        this.success = false;
    }

    @Override
    public String toString() {
        return "Response{" +
                "success=" + success +
                ", data=" + data +
                ", errCode='" + errCode + '\'' +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }

    public static Response success() {
        return new Response();
    }

    public static <T> Response<T> success(T data) {
        Response response = new Response();
        response.setData(data);
        return response;
    }

    public static Response fail() {
        Response response = new Response(FAIL_CODE, FAIL_MSG);
        response.setSuccess(false);
        return response;
    }

    public static Response fail(String msg) {
        Response response = new Response(FAIL_CODE, msg);
        response.setSuccess(false);
        return response;
    }


    public static Response fail(String errCode, String errMsg) {
        Response response = new Response(errCode, errMsg);
        response.setSuccess(false);
        return response;
    }

}
