package com.example.eventbus;

import com.yln.common.dto.message.GenericEvent;
import com.yln.eventbus.core.GenericSubscriber;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

/**
 * @description
 * @author: xiaoyulin
 * @create: 2021-05-18 11:48
 **/
@Log4j2
@Component("medalAddSubscriber")
public class MedalAddSubscriber implements GenericSubscriber {

    @Override
    public void onEvent(GenericEvent event) {
//        System.out.println("Add medal,event:" + event);
        log.info("Add medal,event:" + event);
    }
}
