package com.example.eventbus;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;
import com.yln.common.dto.message.GenericEvent;
import com.yln.eventbus.core.GenericSubscriber;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

/**
 * @description
 * @author: xiaoyulin
 * @create: 2021-05-18 11:48
 **/
@Log4j2
@Component("pointsAddSubscriber")
public class PointsAddSubscriber implements GenericSubscriber {


    @Override
    public void onEvent(GenericEvent event) {
//        System.out.println("Add points,event:" + event);
        log.info("Add points,event:" + event);
    }
}
