package com.example.eventbus;

import com.yln.eventbus.core.AbstractAsyncEventBus;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @description
 * @author: xiaoyulin
 * @create: 2021-05-18 11:42
 **/
@Component("userRegisterEventBus")
public class UserRegisterEventBus extends AbstractAsyncEventBus {

    @Resource
    private PointsAddSubscriber pointsAddSubscriber;

    @Resource
    private MedalAddSubscriber medalAddSubscriber;

    @Override
    public void afterPropertiesSet() throws Exception{
        this.register(pointsAddSubscriber);
        this.register(medalAddSubscriber);
    }

    @Override
    public String getEventType() {
        return "USER_REGISTER";
    }
}
