package com.example.api;

/**
 * @description
 * @author: xiaoyulin
 * @create: 2021-05-10 19:24
 **/
public interface UserApiService {

    String getUser();
}
