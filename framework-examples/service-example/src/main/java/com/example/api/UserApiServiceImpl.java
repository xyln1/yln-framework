package com.example.api;


import org.apache.dubbo.config.annotation.DubboService;

/**
 * @description
 * @author: xiaoyulin
 * @create: 2021-05-10 19:20
 **/
@DubboService
public class UserApiServiceImpl implements UserApiService {


    @Override
    public String getUser() {
        return "test user";
    }
}
