package com.example.test.curator;

import com.example.Application;
import com.yln.curator.client.AbstractZookeeperLock;
import com.yln.curator.client.CuratorClient;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author xiaoyulin
 * @description Zookeeper测试
 * @date 2020-04-07
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
public class ZookeeperTest {

    @Resource
    private CuratorClient curatorClient;

    @Test
    public void testLock(){
        String result = curatorClient.lock(new AbstractZookeeperLock<String>() {

            @Override
            public String getLockPath() {
                return "/lock";
            }

            @Override
            public String execute() {
                return "Hello world!";
            }
        });

        Assert.assertNotNull(result);
    }
}
