package com.example.test.eventbus;

import com.example.Application;
import com.yln.eventbus.deliver.MessageDeliver;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @description
 * @author: xiaoyulin
 * @create: 2021-05-17 20:29
 **/
@SpringBootTest(classes = {Application.class})
@RunWith(SpringRunner.class)
public class MessageDeliverTest {

    @Autowired
    private MessageDeliver messageDeliver;

    @Test
    public void test(){
        messageDeliver.delive("{\n" +
                "    \"eventType\":\"USER_REGISTER\",\n" +
                "    \"objectId\":10,\n" +
                "    \"objectType\":\"USER\",\n" +
                "}");

        messageDeliver.delive("{\n" +
                "    \"eventType\":\"USER_REGISTER\",\n" +
                "    \"objectId\":11,\n" +
                "    \"objectType\":\"USER\",\n" +
                "}");
    }
}
