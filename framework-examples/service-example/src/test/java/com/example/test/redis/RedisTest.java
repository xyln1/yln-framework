package com.example.test.redis;

import com.example.Application;
import com.yln.data.redis.helper.RedisHelper;
import lombok.Data;
import lombok.ToString;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

/**
 * @description
 * @author: xiaoyulin
 * @create: 2021-05-08 18:04
 **/
@SpringBootTest(classes = {Application.class})
@RunWith(SpringRunner.class)
public class RedisTest {

    @Autowired
    private RedisHelper redisHelper;

    @Test
    public void test() {
        redisHelper.set("yln-user", new User(1, "张三"), 100);
        System.out.println(redisHelper.ttl("yln-user"));
        User user = redisHelper.get("yln-user");
        Assert.assertNotNull(user);
    }

    @Test
    public void testHash(){
        String key = "yln-hash1";
        redisHelper.hset(key, "hash11", new User(1, "张三"));
        redisHelper.hset(key, "hash12", new User(2, "李四"));

        String key2 = "yln-hash2";
        redisHelper.hset(key2, "hash21", 1);
        redisHelper.hset(key2, "hash22", 1);

        Map<String, User> map = redisHelper.entries(key);
        System.out.println(redisHelper.hsize(key2));

        Assert.assertNotNull(redisHelper.hget(key, "hash12"));
        Assert.assertNotNull(redisHelper.hget(key2, "hash21"));
    }

    @Test
    public void testZset(){
        String key = "yln-zset";
//        redisHelper.zAdd(key, new User(1, "张三"), 2.0);
//        redisHelper.zAdd(key, new User(2, "李四"), 2.0);
//        redisHelper.zAdd(key, new User(3, "王老五"), 2.0);
        redisHelper.zIncr(key, new User(1, "张三"), 3.0);
        redisHelper.zIncr(key, new User(3, "王老五"), 3.0);
        System.out.println(redisHelper.zRange(key, 0, -1));
        System.out.println(redisHelper.zRevRange(key, 0, -1));
        System.out.println(redisHelper.zrevrank(key, new User(1, "张三")));
        System.out.println(redisHelper.zScore(key, new User(1, "张三")));
    }

    @Test
    public void testZset2(){
        String key = "yln-zset2";
        redisHelper.zAdd(key, String.valueOf(Integer.MAX_VALUE - 1), 2.0);
        redisHelper.zAdd(key, String.valueOf(Integer.MAX_VALUE - 2), 2.0);
        redisHelper.zAdd(key, String.valueOf(Integer.MAX_VALUE - 3), 2.0);
        redisHelper.zAdd(key, String.valueOf(Integer.MAX_VALUE - 4), 9.0);
        redisHelper.zAdd(key, String.valueOf(Integer.MAX_VALUE - 5), 7.0);
        redisHelper.zIncr(key, String.valueOf(Integer.MAX_VALUE - 1), 3.0);
        System.out.println(redisHelper.zIncr(key, String.valueOf(Integer.MAX_VALUE - 3), 3.0));
        System.out.println(redisHelper.zRange(key, 0, -1));
        redisHelper.zremove(key, String.valueOf(Integer.MAX_VALUE - 4), String.valueOf(Integer.MAX_VALUE - 5));
        System.out.println(redisHelper.zRevRange(key, 0, -1));
        System.out.println(redisHelper.zrevrank(key, String.valueOf(Integer.MAX_VALUE - 1)));
        System.out.println(redisHelper.zScore(key, String.valueOf(Integer.MAX_VALUE - 1)));
        redisHelper.zremrange(key, 0, -1);
        System.out.println(redisHelper.zRange(key, 0, -1));
    }

    @Test
    public void testList(){
        String key = "yln-list";
        redisHelper.lpush(key, new User(1, "张三"));
        redisHelper.lpush(key, new User(2, "张三2"));
        redisHelper.lpush(key, new User(3, "张三3"));
        System.out.println(redisHelper.lLen(key));
        System.out.println(redisHelper.lRange(key, 0, 1));
        redisHelper.rpop(key);
        System.out.println((User)redisHelper.lIndex(key, 0L));
        System.out.println(redisHelper.lRange(key, 0, -1));
    }

    @Test
    public void testSet(){
        String key = "yln-set";
        redisHelper.sadd(key, new User(1, "张三"));
        redisHelper.sadd(key, new User(2, "张三2"));
        redisHelper.sadd(key, new User(2, "张三2"));
        redisHelper.sadd(key, new User(3, "张三3"));
        System.out.println(redisHelper.members(key));
        System.out.println(redisHelper.isMember(key, new User(4, "张三4")));
    }

    @ToString
    @Data
    static class User{
        private Integer id;
        private String name;

        public User(){
        }

        public User(Integer id, String name){
            this.id = id;
            this.name = name;
        }
    }
}
