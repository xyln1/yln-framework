

package com.example.test.beans;

import com.yln.common.beans.EnhanceBeanUtils;
import lombok.Data;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.*;


@WebAppConfiguration
@RunWith(SpringRunner.class)
@TestPropertySource(locations = {"classpath:application.properties"})
public class EnhanceBeanUtilsTest {

    @Test
    public void testCopy() {
        UserPo userPo = new UserPo();
        userPo.setAge(10);
        userPo.setName("张三");
        userPo.setInfos(Arrays.asList("aaa", "bbb"));
        userPo.setNums(Arrays.asList(1, 2, 3));
        List<AddressPo> adressList = new LinkedList<>();
        adressList.add(new AddressPo("address1",1));
        adressList.add(new AddressPo("address2",2));
        userPo.setAdressList(adressList);
        Map<String, AddressPo> addressMap = new LinkedHashMap<>();
        addressMap.put("address1", adressList.get(0));
        addressMap.put("address2", adressList.get(1));
        userPo.setAddressMap(addressMap);

        long start = System.currentTimeMillis();
        for(int i=0;i<100000;i++) {
            UserVo userVo = new UserVo();
            EnhanceBeanUtils.copyProperties(userPo, userVo);
//            Assert.assertTrue(userVo.getName() != null);
        }
        long end = System.currentTimeMillis();
        long cost = end - start;
        System.out.print("耗时：" + cost);
    }

    @Data
    public static class UserPo {
        String name;
        int age;
        List<AddressPo> adressList;
        Map<String, AddressPo> addressMap;

        List<String> infos;

        List<Integer> nums;
    }

    @Data
    public static class AddressPo {
        String info;

        Integer num;

        AddressPo() {
        }

        AddressPo(String info) {
            this.info = info;
        }
        AddressPo(String info, Integer num) {
            this.info = info;
            this.num = num;
        }
    }

    @Data
    public static class UserVo {
        String name;
        int age;
        List<AddressVo> adressList;
        Map<String, AddressPo> addressMap;

        List<String> infos;

        List<Integer> nums;
    }

    @Data
    public static class AddressVo {
        String info;

        int num;

    }


}

