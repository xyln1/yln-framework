package com.example.web;

import com.yln.common.web.annotation.EnableJwt;
import com.yln.common.web.annotation.EnableStandardWeb;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @description
 * @author: xiaoyulin
 * @create: 2021-05-07 15:31
 **/
@EnableDubbo
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableJwt
@EnableStandardWeb
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
