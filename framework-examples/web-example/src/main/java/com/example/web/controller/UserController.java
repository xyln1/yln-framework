package com.example.web.controller;

import com.example.api.UserApiService;
import com.yln.common.web.annotation.NoLogin;
import com.yln.common.web.context.JwtUser;
import com.yln.common.web.context.UserThreadContext;
import com.yln.common.web.helper.jwt.LoginManager;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaoyulin
 * @description
 * @date 2020-04-05
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @DubboReference
    private UserApiService userApiService;


    /**
     * request.header:{Jwt-token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI2N2JkMTY5Mi1mNjg2LTQ1YzctOWRmNi00ODUwNzVmMzRmMjciLCJpc3MiOiIiLCJzdWIiOiIiLCJleHAiOjE2MjA0NTk2NzgsIm5iZiI6MTYyMDM3MzI3OCwiVXNlcklkIjoxLCJ0dGxzIjo4NjQwMCwiVXNlclR5cGUiOiIxIn0.K4gTW-ZvArAzcKzk8vMlKGqP8mwBdo5Ik1izOf0gNAM}
     * @return
     */
    @RequestMapping("/info")
    public JwtUser getUser() {
        JwtUser user = (JwtUser) UserThreadContext.getUser();
        return user;
    }

    @NoLogin
    @RequestMapping("/login")
    public String login(){
        System.out.println(userApiService.getUser());;
        return LoginManager.login(1L, "1");
    }

}
