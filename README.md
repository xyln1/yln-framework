# yln-framework

#### 介绍
基于spring boot(2.3.10)的开发框架，旨在为开发者提供方便、快速的系统搭建体验。

#### 软件架构
基于Spring boot，提供业务系统通用的开箱即用的辅助工具。
![image](架构.png)


#### 使用说明

组件说明：

1.  yln-commons

    
    1）深度Bean Copy：EnhanceBeanUtils
    例如：UserPo{name:zhangsan,addressList:List<AddressPo>[{info:'test'}]}<br/>
         copy-->UserVo{name:zhangsan,addressList:List<AddressVo>[{info:'test'}]}
    
    2）ApplicationContext Holder:
    SpringContextHolder.getApplicationContext()即可获取到spring上下文，同时提供获取环境和Bean的方法
   
    3）业务工厂：BusinessFactory<T>   
    根据业务类型，获取相应接口的具体实现
    
2.  components，提供通用项目组件


    1) component-dto
    提供了响应、分页响应、分页查询dto等
    
    2) component-exception
    提供了通用的自定义异常（BizException业务异常、ForbiddenException禁止访问异常、UnauthorizedException无权限异常等），及CodeMsg（错误码枚举接口）
    
    3) component-eventbus
    发布/订阅事件总线，集成了guava的EventBus，提供便捷的消息发送和处理，开发同学只需关心具体事件的业务处理逻辑。
    
    Example：
        
        i.UserRegisterEventBus，实现afterPropertiesSet()注册Subscriber，实现getEventType()关联事件：
        
        ``
        @Component("userRegisterEventBus")
        public class UserRegisterEventBus extends AbstractEventBus {
            @Resource
            private PointsAddSubscriber pointsAddSubscriber;
        
            @Override
            public void afterPropertiesSet() throws Exception{
                this.register(pointsAddSubscriber);
                this.register(medalAddSubscriber);
            }
        
            @Override
            public String getEventType() {
                return "USER_REGISTER";
            }
        }
        
        @Component("pointsAddSubscriber")
        public class PointsAddSubscriber implements GenericSubscriber {
            @Override
            public void onEvent(GenericEvent event) {
                System.out.println("Add points,event:" + event);
            }
        }
        
        ii.消息消费，传入payload：
        
        ``
        messageDeliver.delive("{eventType\":\"USER_REGISTER\",\"objectId\":10,\"objectType\":\"USER\"}");
        
3.  web-starter(web项目依赖starter)

    
    提供快捷web项目搭建体验，提供统一的异常处理、登录鉴权管理、权限拦截。
    1）@EnableStandardWeb，启用ServletComponentScan、RestTemplate、ContextFilter、web异常统一处理器WebExceptionHandler等
    2）@EnableJwt，启用Json Web Token，引入JwtFilter解析jwt-token成线程用户对象、权限拦截器和Jwt登录管理等功能
    

4.  spring-starters，常用spring boot开箱组件
   
   
    1)redis-starter: （
    Redis spring boot开箱组件，提供了方便快捷的接入，有redis常用操作、redis分布式锁等，支持单机和集群模式，可以根据需要选用Lettuce(默认)、Jedis客户端，参照com.example.test.RedisTest
   
    2)curator-starter:
    Curator spring boot开箱组件，有分布式zk锁（com.yln.curator.client.CuratorClient.lock）
   
    3)dubbo-starter:
    Dubbo spring boot开箱组件 

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
