package com.yln.common.context.ids;

import com.yln.common.utils.Base62Util;

/**
 * @description traceId生成
 * @author: xiaoyulin
 * @create: 2021-05-02 11:42
 **/
public final class TraceIdGenerator {

    private static final IdGen INSTANCE = new IdGen();

    public static String generate() {
        return Base62Util.encode62(INSTANCE.next());
    }

    public static void main(String[] args) {
        System.out.println("ID:" + TraceIdGenerator.generate());
    }

}
