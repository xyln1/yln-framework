package com.yln.common.context.annotation;


import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 业务服务适配
 *
 * @author xiaoyulin
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface Business {

    /**
     * 业务类别（用于业务服务分类）
     *
     * @return
     */
    String[] type() default {};

}
