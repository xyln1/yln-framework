package com.yln.common.context;


import com.yln.common.context.annotation.Business;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;

import java.util.*;

/**
 * @author xiaoyulin
 * @description Spring Context Holder
 */
public class SpringContextHolder implements ApplicationContextAware {

    private final static Logger log = LoggerFactory.getLogger(SpringContextHolder.class);

    /**
     * 以静态变量保存ApplicationContext,可在任意代码中取出ApplicaitonContext.
     */
    private static ApplicationContext context;

    /**
     * 实现ApplicationContextAware接口的context注入函数, 将其存入静态变量.
     */
    @Override
    public void setApplicationContext(ApplicationContext context) {
        SpringContextHolder.context = context;
    }

    public static ApplicationContext getApplicationContext() {
        return context;
    }

    /**
     * 从静态变量ApplicationContext中取得Bean, 自动转型为所赋值对象的类型.
     */
    @SuppressWarnings("unchecked")
    public static <T> T getBean(String name) {
        return (T) context.getBean(name);
    }

    public static <T> T getBean(Class<T> classT) {
        return context.getBean(classT);
    }

    public static <T> T getBean(String name, Class<T> classT) {
        return context.getBean(name, classT);
    }

    public static Environment getEnvironment() {
        return context.getEnvironment();
    }

    public static String getContextPath(){
        String  contextPath = getEnvironment().getProperty("server.servlet.context-path");
        return StringUtils.isNotBlank(contextPath) ? contextPath : "";
    }

//    /**
//     * 获取业务服务Map<业务类别, T>
//     *
//     * @param clazz
//     * @param <T>
//     * @return
//     * @throws BeansException
//     */
//    public static <T> Map<String, T> getBusiness(Class<T> clazz) throws BeansException {
//        Map<String, T> businessMap = new HashMap<>();
//        Map<String, T> result = context.getBeansOfType(clazz);
//        if (result != null) {
//            log.info("Business clazz:{},size:{}", clazz, result.size());
//            Iterator<String> it = result.keySet().iterator();
//            String key = null;
//            while (it.hasNext()) {
//                key = it.next();
//                T t = result.get(key);
//                Business business = t.getClass().getAnnotation(Business.class);
//                if (business != null) {
//                    String[] types = business.type();
//                    log.info("Business-key:{},type:{}", key, types);
//                    for (String type : types) {
//                        businessMap.put(type, t);
//                    }
//                }
//            }
//        }
//        return bussinessMap;
//    }

    /**
     * 获取业务服务Map<业务类别,  List<T>>
     *
     * @param clazz
     * @param <T>
     * @return
     * @throws BeansException
     */
    public static <T> Map<String, List<T>> getBusiness(Class<T> clazz) throws BeansException {
        Map<String, List<T>> businessMap = new HashMap<>();
        Map<String, T> result = context.getBeansOfType(clazz);
        if (result != null) {
            log.info("Business clazz:{},size:{}", clazz, result.size());
            Iterator<String> it = result.keySet().iterator();
            String key = null;
            while (it.hasNext()) {
                key = it.next();
                T t = result.get(key);
                Business business = t.getClass().getAnnotation(Business.class);
                if (business != null) {
                    String[] types = business.type();
                    log.info("Business-key:{},type:{}", key, types);
                    List<T> businessList = null;
                    for (String type : types) {
                        businessList = businessMap.get(type);
                        if(businessList == null){
                            businessList = new ArrayList<>();
                            businessMap.put(type, businessList);
                        }
                        businessList.add(t);
                    }
                }
            }
        }
        return businessMap;
    }
}
