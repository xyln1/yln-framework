package com.yln.common.context.annotation;

import com.yln.common.context.config.SpringContextConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启用SpringContext
 *
 * @author xiaoyulin
 */
@Deprecated
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(SpringContextConfiguration.class)
public @interface EnableSpringContext {


}
