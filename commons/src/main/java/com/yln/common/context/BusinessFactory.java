package com.yln.common.context;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

/**
 * 差异化业务服务工厂
 * @author xiaoyulin
 */
public class BusinessFactory<T> {

    private static final Logger LOG = LoggerFactory.getLogger(BusinessFactory.class);

    /**
     * 业务服务Map（key：type,value：bean）
     */
    private volatile Map<String, List<T>> businessMap = null;

    /**
     * 根据类型获取业务实体列表
     * @param type
     * @return bean
     */
    public List<T> getBusinessList(String type) {
        if (StringUtils.isEmpty(type)) {
            return null;
        }

        if (businessMap == null) {
            synchronized (this) {
                if (businessMap == null) {
                    init();
                }
            }
        }
        List<T> businessList = businessMap.get(type);
        LOG.info("type:" + type + ",business size:" + (businessList==null?0:businessList.size()));
        return businessList;
    }

    /**
     * 根据类型获取业务实体
     * @param type
     * @return bean
     */
    public T getBusiness(String type) {
        List<T> businessList = this.getBusinessList(type);
        if(CollectionUtils.isEmpty(businessList)){
            return null;
        }
        return businessList.get(0);
    }

    private void init() {
        businessMap = SpringContextHolder.getBusiness(this.getTClass());
        LOG.info("businessMap.size:" + (businessMap == null ? 0 : businessMap.size()));
    }

    /**
     * 实际接口在ActualTypeArguments位置，默认第一个（index：0）<br/>
     * 可以重写此方法，指定相应接口
     * @return
     */
    protected int getActualTypeIndex() {
        return 0;
    }

    public Class<T> getTClass() {
        Class<T> tClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[getActualTypeIndex()];
        return tClass;
    }

}
