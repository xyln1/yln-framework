package com.yln.common.utils;


import java.util.Arrays;
import java.util.Objects;
import java.util.stream.IntStream;

/**
 * 数组相关工具类
 *
 */
public final class ArrayUtil {

    private ArrayUtil() {
        throw new UnsupportedOperationException("Unsupported Operation");
    }

    /**
     * Index Not Found
     */
    public static final int INDEX_NOT_FOUND = -1;

    /**
     * Finds the index of the given object in the array.
     *
     * @param array
     * @param objectToFind
     * @param startInclusive
     * @return
     */
    public static int indexOf(final Object[] array, final Object objectToFind, int startInclusive) {
        if (array == null) {
            return INDEX_NOT_FOUND;
        }

        startInclusive = startInclusive < 0 ? 0 : startInclusive;

        return IntStream.range(startInclusive, array.length)
                .filter(i -> Objects.equals(objectToFind, array[i]))
                .min()
                .orElse(INDEX_NOT_FOUND);
    }

    /**
     * Finds the index of the given object in the array.
     *
     * @param array
     * @param objectToFind
     * @return
     */
    public static int indexOf(final Object[] array, final Object objectToFind) {
        return indexOf(array, objectToFind, 0);
    }

    /**
     * Finds the index of the given value in the array.
     *
     * @param array
     * @param valueToFind
     * @param startInclusive
     * @return
     */
    public static int indexOf(final long[] array, final long valueToFind, int startInclusive) {
        if (array == null) {
            return INDEX_NOT_FOUND;
        }

        startInclusive = startInclusive < 0 ? 0 : startInclusive;

        return IntStream.range(startInclusive, array.length)
                .filter(i -> valueToFind == array[i])
                .min()
                .orElse(INDEX_NOT_FOUND);
    }

    /**
     * Finds the index of the given value in the array.
     *
     * @param array
     * @param valueToFind
     * @return
     */
    public static int indexOf(final long[] array, final long valueToFind) {
        return indexOf(array, valueToFind, 0);
    }

    /**
     * Finds the index of the given value in the array.
     *
     * @param array
     * @param valueToFind
     * @param startInclusive
     * @return
     */
    public static int indexOf(final int[] array, final int valueToFind, int startInclusive) {
        if (array == null) {
            return INDEX_NOT_FOUND;
        }

        startInclusive = startInclusive < 0 ? 0 : startInclusive;

        return IntStream.range(startInclusive, array.length)
                .filter(i -> valueToFind == array[i])
                .min()
                .orElse(INDEX_NOT_FOUND);
    }

    /**
     * Finds the index of the given value in the array.
     *
     * @param array
     * @param valueToFind
     * @return
     */
    public static int indexOf(final int[] array, final int valueToFind) {
        return indexOf(array, valueToFind, 0);
    }

    /**
     * Finds the index of the given value in the array.
     *
     * @param array
     * @param valueToFind
     * @param startInclusive
     * @return
     */
    public static int indexOf(final double[] array, final double valueToFind, int startInclusive) {
        if (array == null) {
            return INDEX_NOT_FOUND;
        }

        startInclusive = startInclusive < 0 ? 0 : startInclusive;

        return IntStream.range(startInclusive, array.length)
                .filter(i -> valueToFind == array[i])
                .min()
                .orElse(INDEX_NOT_FOUND);
    }

    /**
     * Finds the index of the given value in the array.
     *
     * @param array
     * @param valueToFind
     * @return
     */
    public static int indexOf(final double[] array, final double valueToFind) {
        return indexOf(array, valueToFind, 0);
    }

    /**
     * Checks if the object is in the given array.
     *
     * @param array
     * @param objectToFind
     * @return
     */
    public static boolean contains(final Object[] array, final Object objectToFind) {

        if (array == null) {
            return false;
        }

        return Arrays.stream(array)
                .anyMatch(object -> Objects.equals(objectToFind, object));
    }

    /**
     * Checks if the value is in the given array.
     *
     * @param array
     * @param valueToFind
     * @return
     */
    public static boolean contains(final long[] array, final long valueToFind) {
        if (array == null) {
            return false;
        }

        return Arrays.stream(array)
                .anyMatch(value -> value == valueToFind);
    }

    /**
     * Checks if the value is in the given array.
     *
     * @param array
     * @param valueToFind
     * @return
     */
    public static boolean contains(final int[] array, final int valueToFind) {
        if (array == null) {
            return false;
        }

        return Arrays.stream(array)
                .anyMatch(value -> value == valueToFind);
    }

    /**
     * Checks if the value is in the given array.
     *
     * @param array
     * @param valueToFind
     * @return
     */
    public static boolean contains(final double[] array, final double valueToFind) {
        if (array == null) {
            return false;
        }

        return Arrays.stream(array)
                .anyMatch(value -> value == valueToFind);
    }
}
