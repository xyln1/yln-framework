package com.yln.common.utils;

/**
 * @description
 * @author: xiaoyulin
 * @create: 2021-05-02 14:40
 **/
public class Base62Util {

    public static String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    /**
     * base62编码
     * @param num
     * @return
     */
    public static String encode62(long num) {
        if (num < 1) {
            throw new IllegalArgumentException("num must be greater than 0.");
        }
        StringBuilder sb = new StringBuilder();
        for (; num > 0; num /= 62) {
            sb.append(ALPHABET.charAt((int) (num % 62)));
        }
        return sb.toString();
    }

    /**
     * base62解码
     * @param str
     * @return
     */
    public static long decode62(String str) {
        if (str == null || str.trim().length() == 0) {
            throw new IllegalArgumentException("str must not be empty.");
        }
        long result = 0;
        for (int i = 0; i < str.length(); i++) {
            result += ALPHABET.indexOf(str.charAt(i)) * Math.pow(62, i);
        }
        return result;
    }
}
