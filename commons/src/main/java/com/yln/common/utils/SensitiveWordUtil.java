package com.yln.common.utils;

import java.util.*;

/**
 * @author xiaoyulin
 * @description 敏感词处理 - DFA算法实现
 * @date 2020-03-23
 */
public class SensitiveWordUtil {

    /**
     * 最小匹配规则，如：敏感词库["中国","中国人"]，语句："我是中国人"，匹配结果：我是[中国]人
     */
    public static final int MATCH_MIN = 0;

    /**
     * 最大匹配规则，如：敏感词库["中国","中国人"]，语句："我是中国人"，匹配结果：我是[中国人]
     */
    public static final int MATCH_MAX = 1;

    /**
     * 敏感词集合
     */
    public static Map sensitiveWordMap;

    /**
     * 初始化敏感词库，构建DFA算法模型
     *
     * @param sensitiveWordSet 敏感词库
     */
    public static synchronized void init(Set<String> sensitiveWordSet) {
        initSensitiveWordMap(sensitiveWordSet);
    }

    public static synchronized void addSensitiveWord(Set<String> sensitiveWordSet) {
        initSensitiveWordMap(sensitiveWordSet);
    }

    /**
     * 初始化敏感词库，构建DFA算法模型
     *
     * @param sensitiveWordSet 敏感词库
     */
    private static void initSensitiveWordMap(Set<String> sensitiveWordSet) {
        //初始化敏感词容器，减少扩容操作
        if (sensitiveWordMap == null) {
            sensitiveWordMap = new HashMap(sensitiveWordSet.size());
        }
        String key;
        Map nowMap;
        Map<String, String> newWorMap;
        //迭代sensitiveWordSet
        Iterator<String> iterator = sensitiveWordSet.iterator();
        while (iterator.hasNext()) {
            //关键字
            key = iterator.next();
            nowMap = sensitiveWordMap;
            for (int i = 0; i < key.length(); i++) {
                //转换成char型
                char keyChar = key.charAt(i);
                //库中获取关键字
                Object wordMap = nowMap.get(keyChar);
                //如果存在该key，直接赋值，用于下一个循环获取
                if (wordMap != null) {
                    nowMap = (Map) wordMap;
                } else {
                    //不存在则，则构建一个map，同时将isEnd设置为0，因为他不是最后一个
                    newWorMap = new HashMap<>();
                    //不是最后一个
                    nowMap.put(keyChar, newWorMap);
                    nowMap = newWorMap;
                }

            }
        }
    }

    /**
     * 判断文字是否包含敏感字符
     *
     * @param txt       文字
     * @param matchType 匹配规则 1：最小匹配规则，2：最大匹配规则
     * @return 若包含返回true，否则返回false
     */
    public static boolean contains(String txt, int matchType) {
        boolean flag = false;
        for (int i = 0; i < txt.length(); i++) {
            int matchFlag = checkSensitiveWord(txt, i, matchType); //判断是否包含敏感字符
            if (matchFlag > 0) {    //大于0存在，返回true
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 判断文字是否包含敏感字符
     *
     * @param txt 文字
     * @return 若包含返回true，否则返回false
     */
    public static boolean contains(String txt) {
        return contains(txt, MATCH_MAX);
    }

    /**
     * 获取文字中的敏感词
     *
     * @param txt       文字
     * @param matchType 匹配规则 1：最小匹配规则，2：最大匹配规则
     * @return
     */
    public static List<String> getSensitiveWord(String txt, int matchType) {
        Set<String> sensitiveWordSet = new HashSet<>();

        for (int i = 0; i < txt.length(); i++) {
            //判断是否包含敏感字符
            int length = checkSensitiveWord(txt, i, matchType);
            if (length > 0) {//存在,加入list中
                sensitiveWordSet.add(txt.substring(i, i + length));
                i = i + length - 1;//减1的原因，是因为for会自增
            }
        }

        List<String> sensitiveWordList = new ArrayList<>();
        sensitiveWordSet.forEach((x) -> sensitiveWordList.add(x));

        return sensitiveWordList;
    }

    /**
     * 敏感词排序（按长度从长到短）
     *
     * @param sensitiveWordSet
     * @return
     */
    private static List<String> toSortList(Set<String> sensitiveWordSet) {
        Iterator<String> iterator = sensitiveWordSet.iterator();
        Map<Integer, List<String>> bucketMap = new HashMap<>();
        String sensitiveWord = null;
        List tempList = null;
        int maxLen = 0;
        while (iterator.hasNext()) {
            sensitiveWord = iterator.next();
            if (sensitiveWord.length() > maxLen) {
                maxLen = sensitiveWord.length();
            }
            if (bucketMap.get(sensitiveWord.length()) == null) {
                tempList = new ArrayList<String>();
                tempList.add(sensitiveWord);
                bucketMap.put(sensitiveWord.length(), tempList);
            } else {
                bucketMap.get(sensitiveWord.length()).add(sensitiveWord);
            }
            sensitiveWord = null;
            tempList = null;
        }

        List<String> sensitiveWordList = new ArrayList<>();
        for (int i = maxLen; i > 1; i--) {
            sensitiveWordList.addAll(bucketMap.get(i));
        }
        return sensitiveWordList;
    }

    /**
     * 获取文字中的敏感词
     *
     * @param txt 文字
     * @return
     */
    public static List<String> getSensitiveWord(String txt) {
        return getSensitiveWord(txt, MATCH_MAX);
    }

    /**
     * 替换敏感字字符
     *
     * @param txt         文本
     * @param replaceChar 替换的字符，匹配的敏感词以字符逐个替换，如 语句：我爱中国人 敏感词：中国人，替换字符：*， 替换结果：我爱***
     * @param matchType   敏感词匹配规则
     * @return
     */
    public static String replaceSensitiveWord(String txt, char replaceChar, int matchType) {
        String resultTxt = txt;
        //获取所有的敏感词
        List<String> list = getSensitiveWord(txt, matchType);
        String replaceString = null;
        for (String word : list) {
            replaceString = getReplaceChars(replaceChar, word.length());
            resultTxt = resultTxt.replaceAll(word, replaceString);
        }
        return resultTxt;
    }

    /**
     * 替换敏感字字符
     *
     * @param txt         文本
     * @param replaceChar 替换的字符，匹配的敏感词以字符逐个替换，如 语句：我爱中国人 敏感词：中国人，替换字符：*， 替换结果：我爱***
     * @return
     */
    public static String replaceSensitiveWord(String txt, char replaceChar) {
        return replaceSensitiveWord(txt, replaceChar, MATCH_MAX);
    }

    /**
     * 替换敏感字字符
     *
     * @param txt        文本
     * @param replaceStr 替换的字符串，匹配的敏感词以字符逐个替换，如 语句：我爱中国人 敏感词：中国人，替换字符串：[屏蔽]，替换结果：我爱[屏蔽]
     * @param matchType  敏感词匹配规则
     * @return
     */
    public static String replaceSensitiveWord(String txt, String replaceStr, int matchType) {
        String resultTxt = txt;
        List<String> list = getSensitiveWord(txt, matchType);
        for (String word : list) {
            resultTxt = resultTxt.replaceAll(word, replaceStr);
        }

        return resultTxt;
    }

    /**
     * 替换敏感字字符
     *
     * @param txt        文本
     * @param replaceStr 替换的字符串，匹配的敏感词以字符逐个替换，如 语句：我爱中国人 敏感词：中国人，替换字符串：[屏蔽]，替换结果：我爱[屏蔽]
     * @return
     */
    public static String replaceSensitiveWord(String txt, String replaceStr) {
        return replaceSensitiveWord(txt, replaceStr, MATCH_MAX);
    }

    /**
     * 获取替换字符串
     *
     * @param replaceChar
     * @param length
     * @return
     */
    private static String getReplaceChars(char replaceChar, int length) {
        String resultReplace = String.valueOf(replaceChar);
        for (int i = 1; i < length; i++) {
            resultReplace += replaceChar;
        }

        return resultReplace;
    }

    /**
     * 检查文字中是否包含敏感字符，检查规则如下：<br>
     *
     * @param txt
     * @param beginIndex
     * @param matchType
     * @return 如果存在，则返回敏感词字符的长度，不存在返回0
     */
    private static int checkSensitiveWord(String txt, int beginIndex, int matchType) {
        //敏感词结束标识位：用于敏感词只有1位的情况
        boolean flag = false;
        //匹配标识数默认为0
        int matchFlag = 0;
        char word;
        Map nowMap = sensitiveWordMap;
        for (int i = beginIndex; i < txt.length(); i++) {
            word = txt.charAt(i);
            //获取指定key
            nowMap = (Map) nowMap.get(word);
            if (nowMap != null) {//存在，则判断是否为最后一个
                //找到相应key，匹配标识+1
                matchFlag++;
                //如果为最后一个匹配规则,结束循环，返回匹配标识数
                if (nowMap.size() == 0) {
                    //结束标志位为true
                    flag = true;
                    break;
                }
                //最小规则，直接返回,最大规则还需继续查找
                if (matchFlag > 1 && MATCH_MIN == matchType) {
                    flag = true;
                    break;
                }
            } else {//不存在，直接返回
                if (matchFlag > 1) {
                    flag = true;
                }
                break;
            }
        }
        if (matchFlag < 2 || !flag) {//长度必须大于1，为词
            matchFlag = 0;
        }
        return matchFlag;
    }

    public static void main(String[] args) {
        Set<String> sensitiveWordSet = new HashSet<>();
        sensitiveWordSet.add("共和国");
        sensitiveWordSet.add("共产");
        sensitiveWordSet.add("政府");
        sensitiveWordSet.add("万万岁");
        sensitiveWordSet.add("万岁");
        //初始化敏感词库
        SensitiveWordUtil.init(sensitiveWordSet);

        System.out.println("敏感词的数量：" + SensitiveWordUtil.sensitiveWordMap.size());
        String string = "中华人民共和国万岁，伟大的共产党万岁，中央人民政府万岁。万岁万岁万万岁。";
        System.out.println("待检测语句字数：" + string.length());

        //是否含有关键字
        boolean result = SensitiveWordUtil.contains(string);
        System.out.println(result);
        result = SensitiveWordUtil.contains(string, SensitiveWordUtil.MATCH_MAX);
        System.out.println(result);

        //获取语句中的敏感词
        List<String> list = SensitiveWordUtil.getSensitiveWord(string);
        System.out.println("语句中包含敏感词的个数为：" + list.size() + "。包含：" + list);
        list = SensitiveWordUtil.getSensitiveWord(string, SensitiveWordUtil.MATCH_MIN);
        System.out.println("语句中包含敏感词的个数为：" + list.size() + "。包含：" + list);

        //替换语句中的敏感词
        String filterStr = SensitiveWordUtil.replaceSensitiveWord(string, '*');
        System.out.println(filterStr);

        filterStr = SensitiveWordUtil.replaceSensitiveWord(string, '*', SensitiveWordUtil.MATCH_MAX);
        System.out.println(filterStr);

        String filterStr2 = SensitiveWordUtil.replaceSensitiveWord(string, "[*敏感词*]");
        System.out.println(filterStr2);

        Set<String> sensitiveWordSet2 = new HashSet<>();
        sensitiveWordSet2.add("万岁。");
        SensitiveWordUtil.addSensitiveWord(sensitiveWordSet2);
        filterStr2 = SensitiveWordUtil.replaceSensitiveWord(string, "[*敏感词*]", SensitiveWordUtil.MATCH_MIN);
        System.out.println(filterStr2);
    }


}
