package com.yln.common.web.exception;

import com.yln.common.dto.Response;
import com.yln.common.exception.BizException;
import com.yln.common.exception.ForbiddenException;
import com.yln.common.exception.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.ConstraintViolationException;
import java.io.FileNotFoundException;

/**
 * @description web接口异常处理
 * @author: xiaoyulin
 * @create: 2021-05-02 15:40
 **/
@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class WebExceptionHandler {

    private final static Logger log = LoggerFactory.getLogger(WebExceptionHandler.class);

    /**
     * 业务异常
     * @param ex
     * @return
     */
    @ExceptionHandler(BizException.class)
    @ResponseStatus(HttpStatus.OK)
    public Response<Object> bizException(BizException ex) {
        log.warn("BizException:{}", ex);
        return Response.fail(ex.getErrorCode(),ex.getMessage());
    }

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Response<Object> unauthorizedException(UnauthorizedException ex) {
        log.warn("UnauthorizedException", ex);
        return Response.fail(String.valueOf(HttpStatus.UNAUTHORIZED.value()),HttpStatus.UNAUTHORIZED.getReasonPhrase());
    }

    @ExceptionHandler(ForbiddenException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Response<Object> handleAccessForbiddenException(ForbiddenException ex) {
        log.warn("FORBIDDEN {}", ex);
        return Response.fail(String.valueOf(HttpStatus.FORBIDDEN.value()),HttpStatus.FORBIDDEN.getReasonPhrase());
    }

    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.OK)
    public Response<Object> handleBindException(BindException ex) {
        String message = CollectionUtils.isEmpty(ex.getFieldErrors()) ? "" : ex.getFieldErrors().toString();
        log.warn("BindException ", ex);
        return Response.fail(message);
    }

    /** 请求错误 start */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response<Object> methodArgumentNotValidException(MethodArgumentNotValidException ex) {
        String message = ex.getBindingResult().getFieldError().getDefaultMessage();
        log.warn("methodArgumentNotValidException 请求参数错误，请检查请求参数", ex);
        return Response.fail(String.valueOf(HttpStatus.BAD_REQUEST.value()),message);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response<Object> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex) {
        log.warn("handleHttpMessageNotReadableException 请求参数错误", ex);
        return Response.fail(String.valueOf(HttpStatus.BAD_REQUEST.value()),"参数错误");
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected Response<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex, WebRequest request) {
        String message = String.format("The parameter '%s' of value '%s' could not be converted to type '%s'", ex.getName(), ex.getValue(), ex.getRequiredType().getSimpleName());
        log.warn("handleMethodArgumentTypeMismatch 请求错误", ex);
        return Response.fail(String.valueOf(HttpStatus.BAD_REQUEST.value()),message);
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Response<Object> noHandlerFoundException(NoHandlerFoundException ex) {
        log.warn("noHandlerFoundException 请求错误", ex);
        return Response.fail(String.valueOf(HttpStatus.NOT_FOUND.value()),HttpStatus.NOT_FOUND.getReasonPhrase());
    }
    @ExceptionHandler({FileNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Response<Object> notFoundFileException(Exception ex) {
        log.warn("noHandlerFoundException 请求错误", ex);
        return Response.fail(String.valueOf(HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND.getReasonPhrase());
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public Response<Object> httpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException ex) {
        log.warn("httpRequestMethodNotSupportedException 请求错误", ex);
        return Response.fail(String.valueOf(HttpStatus.METHOD_NOT_ALLOWED.value()),HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase());
    }

    @ExceptionHandler(value = {ConstraintViolationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response<Object> constraintViolationException(ConstraintViolationException ex) {
        log.warn("constraintViolationException 响应错误 系统异常 ", ex);
        return Response.fail(String.valueOf(HttpStatus.BAD_REQUEST.value()),ex.getMessage());
    }

    @ExceptionHandler(value = {IllegalArgumentException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response<Object> illegalArgumentException(IllegalArgumentException ex) {
        log.warn("illegalArgumentException 响应错误 系统异常 ", ex);
        return Response.fail(String.valueOf(HttpStatus.BAD_REQUEST.value()),ex.getMessage());
    }

    /** 服务不可用、运行时异常 start */
    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Response<Object> unknownException(Exception ex) {
        log.error("unknownException 响应错误 系统异常", ex);
        return Response.fail("响应错误");
    }

    @ExceptionHandler(value = {RuntimeException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Response<Object> runtimeException(RuntimeException ex) {
        log.error("runtimeException 响应错误 系统异常", ex);
        return Response.fail(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),"响应错误");
    }

}
