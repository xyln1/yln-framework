package com.yln.common.web.helper;

import com.yln.common.context.ids.TraceIdGenerator;
import com.yln.common.web.constant.WebConstants;
import com.yln.common.web.context.UserThreadContext;
import org.slf4j.MDC;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @description trace工具
 * @author: xiaoyulin
 * @create: 2021-05-02 14:46
 **/
public class RequestTraceHelper {

    /**
     * 会话开始
     * @param request
     */
    public static void preHandle(HttpServletRequest request) {
        UserThreadContext.removeAll();

        String traceId = request.getHeader(WebConstants.TRACE_ID);
        traceId = StringUtils.isEmpty(traceId) ? TraceIdGenerator.generate() : traceId;
        String src = request.getHeader(WebConstants.SRC);
        String ip = request.getHeader(WebConstants.IP);
        ip = StringUtils.isEmpty(ip) ? InetAddressHelper.getRequestIp(request) : ip;

        MDC.put(WebConstants.TRACE_ID, traceId);
        MDC.put(WebConstants.SRC, src);
        MDC.put(WebConstants.IP, ip);

        UserThreadContext.setAttribute(WebConstants.TRACE_ID, traceId);
        UserThreadContext.setAttribute(WebConstants.SRC, src);
        UserThreadContext.setAttribute(WebConstants.IP, ip);

        request.setAttribute(WebConstants.TRACE_ID, traceId);
        request.setAttribute(WebConstants.SRC, src);
        request.setAttribute(WebConstants.IP, ip);
    }

    /**
     * 设置用户信息
     * @param request
     * @param userId
     * @param userType
     */
    public static void setRequestUser(HttpServletRequest request,String userId,String userType) {
        MDC.put(WebConstants.USER_ID, userId);
        MDC.put(WebConstants.USER_TYPE, userType);

        UserThreadContext.setAttribute(WebConstants.USER_ID,userId);
        UserThreadContext.setAttribute(WebConstants.USER_TYPE,userType);
    }

    /**
     * 会话结束
     */
    public static void postHandle() {
        UserThreadContext.removeAll();
        MDC.clear();
    }
}
