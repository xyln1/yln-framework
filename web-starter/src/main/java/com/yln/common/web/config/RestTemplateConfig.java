package com.yln.common.web.config;

import com.yln.common.web.config.properties.HttpClientPoolProperties;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.DefaultServiceUnavailableRetryStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.TimeUnit;

/**
 * @description RestTemplate配置类
 * @author: xiaoyulin
 * @create: 2021-04-29 18:25
 **/
public class RestTemplateConfig {

    @Bean
    @ConfigurationProperties(prefix = "rest-template.http")
    public HttpClientPoolProperties httpClientPoolProperties() {
        return new HttpClientPoolProperties();
    }

    @Bean
    public PoolingHttpClientConnectionManager poolingHttpClientConnectionManager(HttpClientPoolProperties poolProperties){
            Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", SSLConnectionSocketFactory.getSocketFactory())
                .build();

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);
        connectionManager.setMaxTotal(poolProperties.getPoolMaxTotal());
        connectionManager.setDefaultMaxPerRoute(poolProperties.getPoolMaxPerRoute());
        new IdleConnectionMonitorThread(connectionManager,poolProperties).start();
        return connectionManager;
    }

    /**
     * httpClient pool 配置
     * 重试策略 RetryHandler为连接池默认策略
     * 1.RetryHandler: 1. 对特定的io异常进行重连，保证可用性，[InterruptedIOException、UnknownHostException、ConnectException、SSLException] 发生这4中异常不重试
     * 2.ServiceUnavailableRetryExec: 返回码为503时进行重试。
     * @return
     */
    @Bean
    public HttpClient httpClient(HttpClientPoolProperties poolProperties, PoolingHttpClientConnectionManager poolingHttpClientConnectionManager) {

        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(poolProperties.getSocketTimeout())
                .setConnectTimeout(poolProperties.getConnectTimeout())
                .setConnectionRequestTimeout(poolProperties.getConnectionRequestTimeout())
                .build();

        return HttpClientBuilder.create()
                .setServiceUnavailableRetryStrategy(new DefaultServiceUnavailableRetryStrategy(poolProperties.getRetryCount(), 10))
                .setRetryHandler(new DefaultHttpRequestRetryHandler(poolProperties.getRetryCount(), poolProperties.getRequestSentRetryEnabled()))
                .setDefaultRequestConfig(requestConfig)
                .setConnectionManager(poolingHttpClientConnectionManager)
                .build();
    }

    @Bean
    public RestTemplate restTemplate(HttpClient httpClient) {
        ClientHttpRequestFactory clientHttpRequestFactory =  new HttpComponentsClientHttpRequestFactory(httpClient);
        return new RestTemplate(clientHttpRequestFactory);
    }

    /**
     * 驱逐空闲连接
     * 传统阻塞I/O模型的一个主要缺点是，只有在I/O操作中被阻塞时，网络套接字才能对I/O事件做出反应。
     * 当一个连接释放回管理器时，它可以保持活动状态，但是它无法监视套接字的状态并对任何I/O事件作出反应。
     * @Link {http://hc.apache.org/httpcomponents-client-ga/tutorial/html/connmgmt.html#d5e418}
     */
    public static class IdleConnectionMonitorThread extends Thread {

        private final HttpClientConnectionManager connMgr;
        HttpClientPoolProperties httpClientPoolProperties;
        private volatile boolean shutdown;

        public IdleConnectionMonitorThread(HttpClientConnectionManager connMgr,HttpClientPoolProperties httpClientPoolProperties) {
            super();
            this.connMgr = connMgr;
            this.httpClientPoolProperties = httpClientPoolProperties;
        }
        @Override
        public void run() {
            try {
                while (!shutdown) {
                    synchronized (this) {
                        if(httpClientPoolProperties.getIdleConnectionExpired() > httpClientPoolProperties.getIdleConnectionCloseInterval()
                                && httpClientPoolProperties.getIdleConnectionExpired() % httpClientPoolProperties.getIdleConnectionCloseInterval() == 0){
                            wait(httpClientPoolProperties.getIdleConnectionCloseInterval() * 1000L);
                        }else{
                            wait(1000L);
                        }
                        // Close expired connections
                        connMgr.closeExpiredConnections();
                        // Optionally, close connections
                        // that have been idle longer than 30 sec
                        connMgr.closeIdleConnections(httpClientPoolProperties.getIdleConnectionExpired(), TimeUnit.SECONDS);
                    }
                }
            } catch (InterruptedException ex) {
                // terminate
            }
        }

        public void shutdown() {
            shutdown = true;
            synchronized (this) {
                notifyAll();
            }
        }

    }

}
