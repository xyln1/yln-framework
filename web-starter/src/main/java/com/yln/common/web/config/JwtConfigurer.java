package com.yln.common.web.config;

import com.yln.common.web.constant.WebConstants;
import com.yln.common.web.filter.JwtFilter;
import com.yln.common.web.helper.jwt.JwtProperties;
import com.yln.common.web.interceptor.AuthorizationInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @description jwt配置
 * @author: xiaoyulin
 * @create: 2021-05-01 18:57
 **/
public class JwtConfigurer implements WebMvcConfigurer {

    private final static Logger LOGGER = LoggerFactory.getLogger(JwtConfigurer.class);

    @Autowired
    private JwtProperties jwtProperties;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration interceptorRegistration =
                registry.addInterceptor(new AuthorizationInterceptor())
                .addPathPatterns(jwtProperties.getAuthPathPatterns());
        if(jwtProperties.getAuthExcludePathPatterns() != null && jwtProperties.getAuthExcludePathPatterns().length > 0) {
            interceptorRegistration.excludePathPatterns(jwtProperties.getAuthExcludePathPatterns());
        }
    }

    @Bean
    @ConfigurationProperties(prefix = "jwt")
    public JwtProperties jwtProperties() {
        return new JwtProperties();
    }

    @Bean
    public FilterRegistrationBean jwtFilterRegister() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(new JwtFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.setOrder(WebConstants.JWT_FILTER_ORDER);
        return filterRegistrationBean;
    }

}
