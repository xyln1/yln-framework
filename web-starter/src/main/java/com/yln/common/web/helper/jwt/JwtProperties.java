package com.yln.common.web.helper.jwt;

import org.apache.commons.lang3.StringUtils;

/**
 * @description
 *  jwt.key=IQlM43ZoWEVXNoPb8VEQ74fB5qW+cgvrWta+yogjyPKUbnS3uaf6W+ucZV1ciYZhNO1lv2M8m2IfLSbZmSlbBw==
 *  jwt.header=Jwt-Token
 *  jwt.timeout=86400
 *
 * @author: xiaoyulin
 * @create: 2021-05-01 16:08
 **/
public class JwtProperties {

    /**
     * 算法
     */
    private static String algorithm;
    /**
     * 对称加密秘钥
     */
    private static String secret;
    /**
     * jwt签发者
     */
    private static String issuer = "";

    /**
     * jwt所面向的用户
     */
    private static String subject = "";
    /**
     * jwt接收方
     */
    private static String[] audience = new String[0];

    /**
     * jwt header
     */
    private static String header = "Jwt-Token";
    /**
     * jwt timeout
     */
    private static Integer timeout = 86400;

    private String[] authPathPatterns = {"/**"};

    private String[] authExcludePathPatterns;

    public String[] getAuthExcludePathPatterns() {
        return authExcludePathPatterns;
    }

    public void setAuthExcludePathPatterns(String excludePathPatternsStr) {
        if(StringUtils.isNotEmpty(excludePathPatternsStr)) {
            authExcludePathPatterns = excludePathPatternsStr.split(",");
        }
    }

    public String[] getAuthPathPatterns() {
        return authPathPatterns;
    }

    public void setAuthPathPatterns(String pathPatternsStr) {
        if(StringUtils.isNotEmpty(pathPatternsStr)) {
            authPathPatterns = pathPatternsStr.split(",");
        }
    }

    public static String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        JwtProperties.algorithm = algorithm;
    }

    public static String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        JwtProperties.secret = secret;
    }

    public static String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        JwtProperties.issuer = issuer;
    }

    public static String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        JwtProperties.subject = subject;
    }

    public static String[] getAudience() {
        return audience;
    }

    public static void setAudience(String[] audience) {
        JwtProperties.audience = audience;
    }

    public static String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        JwtProperties.header = header;
    }

    public static Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        JwtProperties.timeout = timeout;
    }

}
