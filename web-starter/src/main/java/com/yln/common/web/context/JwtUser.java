package com.yln.common.web.context;

import java.util.List;

/**
 * @description Jwt User
 * @author: xiaoyulin
 * @create: 2021-05-01 17:49
 **/
public class JwtUser implements IUser {

    private Long userId = 0L;
    private String userType;
    private List<String> permissions;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }
}
