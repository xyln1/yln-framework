package com.yln.common.web.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yln.common.context.SpringContextHolder;
import com.yln.common.exception.BizException;
import com.yln.common.web.constant.WebConstants;
import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.util.Assert;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Map;

/**
 * @description
 * @author: xiaoyulin
 * @create: 2021-04-30 16:19
 **/
public class RestTemplateHelper {

    private final static Logger LOGGER = LoggerFactory.getLogger(RestTemplateHelper.class);
    private static RestTemplate restTemplate = SpringContextHolder.getBean(RestTemplate.class);
    private static ObjectMapper mapper = SpringContextHolder.getBean(ObjectMapper.class);

//    /**
//     * 应用名
//     */
//    private static String msName = SpringContextHolder.getEnvironment().getProperty("info.name");
    /**
     * 调用日志格式
     */
    private static final String INVOKE_LOG_FORMAT = "调用外部接口url:{} 调用接口耗时 spendTime:{}ms 请求参数:{} 返回结果:{}";
    /**
     * 默认超时时间
     */
    private static final Long SPENDTIME_THRESHOLD = 5000L;

    /**
     * 适用出，入参遵循Java api数据结构规范的接口
     * @param url
     * @param request
     * @param typeReference
     * @param <T>
     * @return
     */
    public <T> T doPost(String url, String request, TypeReference<T> typeReference) {
        try {
            return mapper.readValue(doPost(request, url), typeReference);
        } catch (IOException e) {
            LOGGER.error("doPost parse json error",e);
        }
        return null;
    }

    /**
     * 适用出，入参遵循Java api数据结构规范的接口
     * @param url
     * @param request
     * @param clazz
     * @return
     */
    public <T> T doPost(String url, String request, Class<T> clazz) {
        try {
            return mapper.readValue(doPost(request, url), clazz);
        } catch (IOException e) {
            LOGGER.error("doPost parse json error",e);
        }
        return null;
    }

    /**
     * 适用出，入参遵循Java api数据结构规范的接口
     * @param url
     * @param params
     * @param typeReference
     * @return T
     */
    public <T> T doGet(String url, Map<String,Object> params, TypeReference<T> typeReference) {
        String body = doGet(url,params);
        try {
            return mapper.readValue(body, typeReference);
        } catch (IOException e) {
            LOGGER.error("doPost parse json error",e);
        }
        return null;
    }

    /**
     * POST请求（Object）
     * @param url
     * @param req
     * @return
     */
    public static String doPostJsonObj(String url,Object req) throws JsonProcessingException {
        Assert.notNull(url, "url be null.");
        Assert.notNull(req, "req be null.");
        return doPost(url, mapper.writeValueAsString(req));
    }

    /**
     * POST请求（JSON）
     * @param url
     * @param req
     * @return
     */
    public static String doPost(String url,String req){
        Assert.notNull(url, "url be null.");

        long spendtime = 0L;
        String responseResult = "";
        ResponseEntity<String> rss = null;
        try {
            long beginTime = System.currentTimeMillis();
            HttpEntity<String> formEntity = new HttpEntity<>(req, buildHeaders(MediaType.APPLICATION_JSON_UTF8));
            rss = restTemplate.exchange(url, HttpMethod.POST, formEntity, String.class);
            spendtime = System.currentTimeMillis() - beginTime;
        } catch (RestClientException e) {
            LOGGER.error("调用外部接口失败: error url:{},httpstatus:{}", url, rss.getStatusCode().value(), e);
            throw new BizException("500", "调用外部接口异常!",e);
        }catch (Exception e){
            LOGGER.error("调用外部接口失败: error url:{}", url, e);
            throw new BizException("500", "调用外部接口异常!",e);
        }finally {
            if (spendtime > SPENDTIME_THRESHOLD){
                LOGGER.warn(INVOKE_LOG_FORMAT, url, spendtime, req, responseResult);
            }else {
                LOGGER.info(INVOKE_LOG_FORMAT, url, spendtime, req, responseResult);
            }
        }
        isCheckHttpStatus(rss);
        return rss.getBody();
    }

    /**
     * POST请求（form-data）
     * @param url
     * @param req
     * @return
     */
    public static String doPostFormData(String url, MultiValueMap<String, Object> req){
        Assert.notNull(url, "url be null.");

        long spendtime = 0L;
        String responseResult = "";
        ResponseEntity<String> rss = null;
        try {
            long beginTime = System.currentTimeMillis();
            HttpEntity<String> formEntity = new HttpEntity(req, buildHeaders(MediaType.APPLICATION_FORM_URLENCODED));
            rss = restTemplate.exchange(url, HttpMethod.POST, formEntity, String.class);
            spendtime = System.currentTimeMillis() - beginTime;
        } catch (RestClientException e) {
            LOGGER.error("调用外部接口失败: error url:{},httpstatus:{}", url, rss.getStatusCode().value(),e);
            throw new BizException("500", "调用外部接口异常!",e);
        }catch (Exception e){
            LOGGER.error("调用外部接口失败: error url:{}", url, e);
            throw new BizException("500", "调用外部接口异常!",e);
        }finally {
            if (spendtime > SPENDTIME_THRESHOLD){
                LOGGER.warn(INVOKE_LOG_FORMAT, url, spendtime, req, responseResult);
            }else {
                LOGGER.info(INVOKE_LOG_FORMAT, url, spendtime, req, responseResult);
            }
        }
        isCheckHttpStatus(rss);
        return rss.getBody();
    }

    /**
     * GET请求
     * @param url
     * @param params
     * @return
     */
    public static String doGet(String url, Map<String,Object> params){
        Assert.notNull(url, "url be null.");

        long spendtime = 0L;
        String result = "";
        ResponseEntity<String> rss = null;
        try {
            long beginTime = System.currentTimeMillis();
            HttpEntity<String> formEntity = new HttpEntity<>(buildHeaders(null));
            rss = restTemplate.exchange(url, HttpMethod.GET, formEntity, String.class, params);
            spendtime = System.currentTimeMillis() - beginTime;
        } catch (RestClientException e) {
            LOGGER.error("调用外部接口失败: error url:{},httpstatus:{}", url, rss.getStatusCode().value(), e);
            throw new BizException("500", "调用外部接口异常!",e);
        }catch (Exception e){
            LOGGER.error("调用外部接口失败: error url:{}", url, e);
            throw new BizException("500", "调用外部接口异常!",e);
        }finally {
            printInvokeLog(url, spendtime, params == null ? "":params.toString(), result);
        }
        isCheckHttpStatus(rss);
        return rss.getBody();
    }

    /**
     * 设置请求头
     * @return HttpHeaders
     */
    private static HttpHeaders buildHeaders(MediaType type){
        HttpHeaders headers = new HttpHeaders();
        if(type !=null) {
            headers.setContentType(type);
        }
        headers.add(WebConstants.HEADER_ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add(WebConstants.USER_TYPE, ThreadContext.get(WebConstants.USER_TYPE));
        headers.add(WebConstants.USER_ID, ThreadContext.get(WebConstants.USER_ID));
        headers.add(WebConstants.SRC, ThreadContext.get(WebConstants.SRC));
        headers.add(WebConstants.TRACE_ID, ThreadContext.get(WebConstants.TRACE_ID));
        return headers;
    }

    /**
     * 检查请求状态
     * @param rss
     */
    private static void isCheckHttpStatus(ResponseEntity<String> rss){
        if(rss == null || rss.getStatusCode().equals(HttpStatus.BAD_REQUEST)){
            throw new BizException("400","参数信息异常！");
        }
        if(rss == null || !rss.getStatusCode().equals(HttpStatus.OK)){
            throw new BizException("500","调用服务失败！");
        }
    }

    /**
     * 打印调用日志
     * @param url
     * @param spendtime
     * @param params
     * @param result
     */
    private static void printInvokeLog(String url, Long spendtime, String params, String result){
        if (spendtime > SPENDTIME_THRESHOLD){
            LOGGER.warn(INVOKE_LOG_FORMAT, url, spendtime, params, result);
        }else {
            LOGGER.info(INVOKE_LOG_FORMAT, url, spendtime, params, result);
        }
    }

}
