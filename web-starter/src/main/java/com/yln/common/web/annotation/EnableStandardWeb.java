package com.yln.common.web.annotation;


import com.yln.common.web.config.RestTemplateConfig;
import com.yln.common.web.config.WebContextConfigurer;
import com.yln.common.web.exception.WebExceptionHandler;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description 启用标准Web
 * @author: xiaoyulin
 * @create: 2021-04-29 18:25
 **/
@ServletComponentScan
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Import({RestTemplateConfig.class, WebContextConfigurer.class, WebExceptionHandler.class})
public @interface EnableStandardWeb {
}
