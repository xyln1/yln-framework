package com.yln.common.web.filter;

import com.yln.common.web.helper.RequestTraceHelper;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @description 上下文filter
 * @author: xiaoyulin
 * @create: 2021-05-02 15:28
 **/
public class ContextFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        try {
            RequestTraceHelper.preHandle(httpServletRequest);
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } finally {
            RequestTraceHelper.postHandle();
        }
    }
}
