package com.yln.common.web.filter;

import com.yln.common.web.context.JwtUser;
import com.yln.common.web.context.UserThreadContext;
import com.yln.common.web.helper.jwt.JwtHelper;
import com.yln.common.web.helper.RequestTraceHelper;
import com.yln.common.web.helper.jwt.JwtPayload;
import com.yln.common.web.helper.jwt.JwtProperties;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @description json web token filter
 *
 * @author: xiaoyulin
 * @create: 2021-05-01 18:30
 **/
public class JwtFilter extends OncePerRequestFilter {

    private final static Logger LOGGER = LoggerFactory.getLogger(JwtFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String jwt = request.getHeader(JwtProperties.getHeader());
        try {
            if(!StringUtils.isEmpty(jwt)){
                JwtPayload jwtPayload = JwtHelper.parse(jwt);
                if(jwtPayload !=null) {
                    JwtUser user = new JwtUser();
                    user.setUserId(jwtPayload.getUserId());
                    user.setUserType(jwtPayload.getUserType());
                    user.setPermissions(jwtPayload.getPermissions());
                    UserThreadContext.putUser(user);
                    refreshJwt(response,jwtPayload);
                    RequestTraceHelper.setRequestUser(request, String.valueOf(jwtPayload.getUserId()), jwtPayload.getUserType());
                }
            }
        } catch (Exception e){
            LOGGER.warn("jwt verify fail {}",jwt,e);
        }
        filterChain.doFilter(request, response);
    }

    /**
     * JWT 滑动延长
     * @param response
     * @param jwtPayload
     */
    private void refreshJwt(HttpServletResponse response,JwtPayload jwtPayload){
        long currSeconds = System.currentTimeMillis() / 1000;
        Integer timeout = jwtPayload.getTtls() == 0 ? 3600 : jwtPayload.getTtls();
        // 时效过半的情况,给一个新的jwt
        if ((currSeconds + (timeout>>1)) > jwtPayload.getExp().getTime()/1000L) {
            Long userId = jwtPayload.getUserId();
            String userType = jwtPayload.getUserType();
            List<String> permissions = jwtPayload.getPermissions();
            String jwt = JwtHelper.create(userId, userType, permissions);
            response.setHeader(JwtProperties.getHeader(), jwt);
            LOGGER.info("时效过半,给一个新的jwt, userId:{}", userId);
        }
    }

}
