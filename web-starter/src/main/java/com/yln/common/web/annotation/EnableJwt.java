package com.yln.common.web.annotation;


import com.yln.common.web.config.JwtConfigurer;
import com.yln.common.web.helper.jwt.LoginManager;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description 启用标准Jwt
 * @author: xiaoyulin
 * @create: 2021-04-29 18:25
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Import({JwtConfigurer.class, LoginManager.class})
public @interface EnableJwt {
}
