package com.yln.common.web.helper.jwt;

import com.yln.common.exception.UnauthorizedException;

import java.util.List;

/**
 * @description 登录管理
 * @author: xiaoyulin
 * @create: 2021-05-02 16:30
 **/
public class LoginManager {

    /**
     * login
     * @param userId
     * @param userType
     * @return
     */
    public static String login(Long userId, String userType){
        return login(userId, userType, null);
    }

    /**
     * login
     * @param userId
     * @param userType
     * @param permissions
     * @return
     */
    public static String login(Long userId, String userType, List<String> permissions){
        String jwt = JwtHelper.create(userId, userType, permissions);
        return jwt;
    }


    /**
     * refresh jwt
     * @param oldJwt
     * @return
     */
    public static String refreshJwt(String oldJwt){
        JwtPayload payload = JwtHelper.parse(oldJwt);
        if(payload == null){
            throw new UnauthorizedException("Jwt refresh failed");
        }
        Long userId = payload.getUserId();
        String userType = payload.getUserType();
        String jwt = JwtHelper.create(userId, userType);
        return jwt;
    }
}
