package com.yln.common.web.config.properties;

/**
 * @description http client配置
 * @author: xiaoyulin
 * @create: 2021-04-29 18:20
 **/
public class HttpClientPoolProperties {

    /***
     * 连接上服务器(握手成功)的时间，超出该时间抛出connect timeout
     */
    private	Integer connectTimeout = 5000;

    /**
     * 数据传输的最长时间，超过该时间抛出read timeout
     */
    private Integer socketTimeout = 10000;

    /**
     * 从连接池中获取到连接的最长时间
     */
    private Integer connectionRequestTimeout = 1000;

    /**
     * 连接池最大连接数
     */
    private Integer poolMaxTotal = 100;

    /**
     * 设置每个地址的并发数
     */
    private Integer poolMaxPerRoute = 100;

    /**
     * 重试次数
     */
    private Integer retryCount = 3;

    /**
     * 长连接空闲时长
     */
    private Integer idleConnectionExpired = 10;

    /**
     * 空闲连接剔除，间隔时间（建议设置为为idleConnectionExpired整除的值）
     */
    private Integer idleConnectionCloseInterval = 2;

    /**
     * Whether or not methods that have successfully sent their request will be retried
     */
    private Boolean requestSentRetryEnabled = false;

    public Integer getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(Integer connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public Integer getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(Integer socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    public Integer getConnectionRequestTimeout() {
        return connectionRequestTimeout;
    }

    public void setConnectionRequestTimeout(Integer connectionRequestTimeout) {
        this.connectionRequestTimeout = connectionRequestTimeout;
    }

    public Integer getPoolMaxTotal() {
        return poolMaxTotal;
    }

    public void setPoolMaxTotal(Integer poolMaxTotal) {
        this.poolMaxTotal = poolMaxTotal;
    }

    public Integer getPoolMaxPerRoute() {
        return poolMaxPerRoute;
    }

    public void setPoolMaxPerRoute(Integer poolMaxPerRoute) {
        this.poolMaxPerRoute = poolMaxPerRoute;
    }

    public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    public Integer getIdleConnectionExpired() {
        return idleConnectionExpired;
    }

    public void setIdleConnectionExpired(Integer idleConnectionExpired) {
        this.idleConnectionExpired = idleConnectionExpired;
    }

    public Boolean getRequestSentRetryEnabled() {
        return requestSentRetryEnabled;
    }

    public void setRequestSentRetryEnabled(Boolean requestSentRetryEnabled) {
        this.requestSentRetryEnabled = requestSentRetryEnabled;
    }

    public Integer getIdleConnectionCloseInterval() {
        return idleConnectionCloseInterval;
    }

    public void setIdleConnectionCloseInterval(Integer idleConnectionCloseInterval) {
        this.idleConnectionCloseInterval = idleConnectionCloseInterval;
    }

}
