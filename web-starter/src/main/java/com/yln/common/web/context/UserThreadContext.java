package com.yln.common.web.context;

import java.util.HashMap;
import java.util.Map;

/**
 * @description 用户线程上线文
 * @author: xiaoyulin
 * @create: 2021-05-01 17:54
 **/
public class UserThreadContext {

    private static final ThreadLocal<Map<String, Object>> CTX_HOLDER = new ThreadLocal<>();

    public static final String USER_SESSION_KEY = "UserSession";

    public static Map<String, Object> get() {
        return CTX_HOLDER.get();
    }

    public static Object getAttribute(String attributeName) {
        Map<String, Object> ctx =  CTX_HOLDER.get();
        return ctx == null ? null : ctx.get(attributeName);
    }

    public static void setAttribute(String attributeName, Object value) {
        Map<String, Object> ctx =CTX_HOLDER.get();
        if (ctx == null) {
            ctx = new HashMap<String, Object>();
            CTX_HOLDER.set(ctx);
        }
        ctx.put(attributeName, value);
    }

    public static IUser getUser() {
        return (IUser) getAttribute(USER_SESSION_KEY);
    }

    public static void putUser(IUser user) {
        setAttribute(USER_SESSION_KEY, user);
    }

    public static void removeAll() {
        CTX_HOLDER.remove();
    }
}
