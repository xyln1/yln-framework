package com.yln.common.web.constant;

/**
 * @description web常量
 * @author: xiaoyulin
 * @create: 2021-04-30 19:40
 **/
public class WebConstants {

    public final static int SUCCESS_CODE = 200;
    public final static String SUCCESS_MSG = "OK";

    public final static int FAIL_CODE = 500;
    public final static String FAIL_MSG = "Internal Server Error";

    public final static String SRC = "Src";
    public final static String IP = "Ip";
    public final static String USER_ID = "UserId";
    public final static String USER_TYPE = "UserType";
    public final static String TRACE_ID = "TraceId";
    public final static String HEADER_ACCEPT = "Accept";

    public final static Integer CONTEXT_FILTER_ORDER = -100;
    public final static Integer CORS_FILTER_ORDER = -99;
    public final static Integer JWT_FILTER_ORDER = -98;
}
