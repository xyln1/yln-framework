package com.yln.common.web.helper.jwt;

import com.yln.common.web.helper.jwt.JwtPayload;
import com.yln.common.web.helper.jwt.JwtProperties;
import io.jsonwebtoken.*;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Base64;
import java.util.List;

/**
 * @description json web token helper
 * @author: xiaoyulin
 * @create: 2021-05-01 16:03
 **/
public class JwtHelper {

    /**
     * 创建JWT
     * @param userId
     * @param userType
     * @return
     */
    public static String create(Long userId, String userType) {
        return create(userId, userType, null);
    }

    /**
     * 创建JWT
     * @param userId
     * @param userType
     * @param permissions
     * @return
     */
    public static String create(Long userId, String userType, List<String> permissions) {
        JwtPayload payload = JwtPayload.builder()
                .userId(userId)
                .userType(userType)
                .permissions(permissions)
                .ttls(JwtProperties.getTimeout())
                .iss(JwtProperties.getIssuer())
                .sub(JwtProperties.getSubject())
                .build();
        return create(payload);
    }

    /**
     * 创建JWT
     * @param payload
     * @return
     */
    public static String create(JwtPayload payload) {
        return sign(payload);
    }

    /**
     * JWT签名
     * @param payload
     * @return
     */
    public static String sign(JwtPayload payload) {
        JwtBuilder builder = payload.jwtBuilder()
                .setHeaderParam(JwsHeader.TYPE, "JWT")
                .setHeaderParam(JwsHeader.ALGORITHM, "HS256")
                .signWith(SignatureAlgorithm.HS256, getSigningKey());
        return builder.compact();
    }
    /**
     * 解析JWT
     * @param jsonWebToken
     * @return the {@link Jws Jws} instance that reflects the specified compact Claims JWS string.
     * @throws UnsupportedJwtException  if the {@code claimsJws} argument does not represent an Claims JWS
     * @throws MalformedJwtException    if the {@code claimsJws} string is not a valid JWS
     * @throws SignatureException       if the {@code claimsJws} JWS signature validation fails
     * @throws ExpiredJwtException      if the specified JWT is a Claims JWT and the Claims has an expiration time
     *                                  before the time this method is invoked.
     * @throws IllegalArgumentException if the {@code claimsJws} string is {@code null} or empty or only whitespace
     * @return  @return the {@link JwtPayload Jws}
     */
    public static JwtPayload parse(String jsonWebToken) {
        if (StringUtils.isBlank(jsonWebToken) || jsonWebToken.length() < 64) {
            return null;
        }

        Claims claims = Jwts.parser()
                .setSigningKey(getSigningKey())
                .parseClaimsJws(jsonWebToken)
                .getBody();
        return JwtPayload.of(claims);
    }


    private static Key getSigningKey(){
        byte[] keyBytes = Base64.getDecoder().decode(JwtProperties.getSecret());
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        return new SecretKeySpec(keyBytes, signatureAlgorithm.getJcaName());
    }
}
