package com.yln.common.web.interceptor;

import com.yln.common.exception.ForbiddenException;
import com.yln.common.exception.UnauthorizedException;
import com.yln.common.utils.ArrayUtil;
import com.yln.common.web.annotation.NoLogin;
import com.yln.common.web.annotation.Permission;
import com.yln.common.web.context.JwtUser;
import com.yln.common.web.context.UserThreadContext;
import org.springframework.util.CollectionUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.List;

/**
 * @description 权限拦截器
 * @author: xiaoyulin
 * @create: 2021-05-01 17:46
 **/
public class AuthorizationInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if(!handler.getClass().isAssignableFrom(HandlerMethod.class)){
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method m = handlerMethod.getMethod();

        /** NOT REQUIRED */
        if (m.isAnnotationPresent(NoLogin.class)) {
            return true;
        }

        /**401 Unauthorized*/
        if(!isLogin()){
            throw new UnauthorizedException("unauthorized use or access");
        }
        /**403 Forbidden*/
        if(!isPermission(m)){
            throw new ForbiddenException("user not authorized");
        }

        return true;
    }

    private boolean isLogin(){
        JwtUser user = getUser();
        if(user == null || user.getUserId() == 0){
            return false;
        }
        return true;
    }

    private boolean isPermission(Method m){
        if (m.isAnnotationPresent(Permission.class)) {
            Permission permission = m.getAnnotation(Permission.class);
            if(permission == null || permission.value().length ==0){
                return true;
            }
            JwtUser user = getUser();
            List<String> permissions = user.getPermissions();
            if(user == null || CollectionUtils.isEmpty(permissions)){
                return false;
            }
            for(String perm : permissions){
                if(ArrayUtil.contains(permission.value(), perm)){
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    private JwtUser getUser(){
        return  (JwtUser) UserThreadContext.getUser();
    }

}
