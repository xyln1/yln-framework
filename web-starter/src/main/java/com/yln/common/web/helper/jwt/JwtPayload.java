package com.yln.common.web.helper.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * @description Json web token payload
 * @author: xiaoyulin
 * @create: 2021-05-01 16:05
 **/
public class JwtPayload implements Serializable {

	public final static String CLAIM_USERID= "UserId";
	public final static String CLAIM_TTLS= "ttls";
	public final static String CLAIM_USERTYPE= "UserType";
	public final static String CLAIM_PERMISSION= "Permissions";

	/**
	 * JWT 推荐选用的7个官方字段
	 * iss (issuer)：签发人
	 * exp (expiration time)：过期时间
	 * sub (subject)：主题
	 * aud (audience)：受众
	 * nbf (Not Before)：生效时间
	 * iat (Issued At)：签发时间
	 * jti (JWT ID)：编号
	 */
	private String iss;
	private String sub;
	private String aud;
	private String jti;
	private Date nbf;
	private Date exp;

	/**
	 * custom of playload
	 * ttls : 过期时间
	 * userId : 用户ID
	 * userType : 用户类型
	 * permissions : 用户权限列表
	 */
	private int ttls;
	private long userId;
	private String userType;
	private List<String> permissions;

	public String getIss() {
		return iss;
	}

	public void setIss(String iss) {
		this.iss = iss;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getAud() {
		return aud;
	}

	public void setAud(String aud) {
		this.aud = aud;
	}

	public String getJti() {
		return jti;
	}

	public void setJti(String jti) {
		this.jti = jti;
	}

	public Date getNbf() {
		return nbf;
	}

	public void setNbf(Date nbf) {
		this.nbf = nbf;
	}

	public Date getExp() {
		return exp;
	}

	public void setExp(Date exp) {
		this.exp = exp;
	}

	public int getTtls() {
		return ttls;
	}

	public void setTtls(int ttls) {
		this.ttls = ttls;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public List<String> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<String> permissions) {
		this.permissions = permissions;
	}

	private JwtPayload() {
	}

	private JwtPayload(String iss, String sub, Integer ttls, String aud, Long userId, String userType) {
		this.iss = iss;
		this.sub = sub;
		this.ttls = ttls;
		this.aud = aud;
		this.jti = UUID.randomUUID().toString();
		this.userId = userId;
		this.userType = userType;
	}

	public void setLifecycle(){
		this.nbf = new Date();
		this.exp = new Date(nbf.getTime() + (ttls * 1000L));
	}

	public static JwtPayload of(Claims claims) {
		Assert.notEmpty(claims, "cannot be null.");
		JwtPayload payload = new JwtPayload();
		payload.setUserId((Integer) (claims.get(CLAIM_USERID)));
		payload.setUserType("" + claims.get(CLAIM_USERTYPE));
		payload.setIss(claims.getIssuer());
		payload.setAud(claims.getAudience());
		payload.setJti(claims.getId());
		payload.setTtls((Integer) claims.get(CLAIM_TTLS));
		payload.setSub(claims.getSubject());
		payload.setExp(claims.getExpiration());
		payload.setNbf(claims.getNotBefore());
		return payload;
	}

	public JwtBuilder jwtBuilder() {
		return Jwts.builder()
				.setId(this.getJti())
				.setIssuer(this.getIss())
				.setAudience(this.getAud())
				.setSubject(this.getSub())
				.setExpiration(this.getExp())
				.setNotBefore(this.getNbf())
				.claim(CLAIM_USERID, this.getUserId())
				.claim(CLAIM_TTLS, this.ttls)
				.claim(CLAIM_USERTYPE, this.getUserType())
				.claim(CLAIM_PERMISSION, this.getPermissions());
	}


	public static Builder builder() {
		return new Builder();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o){return true;}
		if (o == null || getClass() != o.getClass()){return false;}
		if (!super.equals(o)){return false;}
		JwtPayload that = (JwtPayload) o;
		return ttls == that.ttls &&
				userId == that.userId &&
				Objects.equals(iss, that.iss) &&
				Objects.equals(sub, that.sub) &&
				Objects.equals(aud, that.aud) &&
				Objects.equals(jti, that.jti) &&
				Objects.equals(nbf, that.nbf) &&
				Objects.equals(exp, that.exp) &&
				Objects.equals(userType, that.userType) &&
				Objects.equals(permissions, that.permissions);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), iss, sub, aud, jti, nbf, exp, ttls, userId, userType, permissions);
	}

	public static class Builder{
		private int ttls;
		private long userId;
		private String userType;
		private List<String> permissions;
		private String iss;
		private String sub;
		private String aud;

		public final Builder ttls(Integer ttls){
			this.ttls = ttls;
			return this;
		}

		public final Builder userId(Long userId){
			this.userId = userId;
			return this;
		}

		public final Builder userType(String userType){
			this.userType = userType;
			return this;
		}

		public final Builder permissions(List<String> permissions){
			this.permissions = permissions;
			return this;
		}

		public final Builder iss(String iss){
			this.iss = iss;
			return this;
		}

		public final Builder sub(String sub){
			this.sub = sub;
			return this;
		}

		public final Builder aud(String aud){
			this.aud = aud;
			return this;
		}

		public final JwtPayload build() {
			JwtPayload jwtPayload = new JwtPayload(iss,sub,ttls,aud,userId,userType);
			jwtPayload.setLifecycle();
			return jwtPayload;
		}
	}
}
