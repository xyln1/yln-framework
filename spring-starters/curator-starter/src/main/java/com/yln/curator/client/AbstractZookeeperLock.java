package com.yln.curator.client;

import java.util.concurrent.TimeUnit;

/**
 * @author xiaoyulin
 * @description 公用锁抽象类
 * @date 2020-04-07
 */
public abstract class AbstractZookeeperLock<T> {
    private static final int TIME_OUT = 5;

    public abstract String getLockPath();

    public abstract T execute();

    public int getTimeout(){
        return TIME_OUT;
    }

    public TimeUnit getTimeUnit(){
        return TimeUnit.SECONDS;
    }
}