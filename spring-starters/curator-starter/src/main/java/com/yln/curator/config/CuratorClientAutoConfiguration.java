package com.yln.curator.config;

/**
 * @author xiaoyulin
 * @description
 * @date 2020-04-07
 */

import com.yln.curator.client.CuratorClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@ConditionalOnProperty(prefix = "spring.zookeeper",
        name = {"enabled"},
        havingValue = "true")
@Configuration
@EnableConfigurationProperties(ZookeeperProperties.class)
public class CuratorClientAutoConfiguration {


    @Bean(initMethod = "init", destroyMethod = "destroy")
    public CuratorClient curatorClient(ZookeeperProperties zookeeperProperties){
        CuratorClient curatorClient = new CuratorClient(zookeeperProperties);
        return curatorClient;
    }
}
