package com.yln.curator.config;

import lombok.Data;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author xiaoyulin
 * @description Zookeeper配置
 * @date 2020-04-07
 */
@ConfigurationProperties(prefix = "spring.zookeeper")
@Data
public class ZookeeperProperties {

    private String server;

    private String basePath;

    /**
     * 连接超时时间(毫秒，默认：15000)
     */
    private int connectionTimeout = 15000;

    /**
     * 会话超时时间(毫秒，默认：60000)
     */
    private int sessionTimeout = 60000;




}
