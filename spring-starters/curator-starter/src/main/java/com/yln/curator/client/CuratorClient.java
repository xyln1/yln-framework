package com.yln.curator.client;

import com.yln.curator.config.ZookeeperProperties;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.curator.retry.ExponentialBackoffRetry;

/**
 * @author xiaoyulin
 * @description Curator Framework Client
 * @date 2020-04-07
 */
@Slf4j
public class CuratorClient {

    private static final int SLEEP_TIME_MS = 1000;
    private static final int MAX_RETRIES = 3;

    @Getter
    private ZookeeperProperties zookeeperProperties;

    @Getter
    private CuratorFramework client;

    public CuratorClient(ZookeeperProperties properties){
        this.zookeeperProperties = properties;
    }

    /**
     * 加分布式锁执行
     * @param zkLock 加锁资源（服务）
     * @param <T>
     * @return
     */
    public <T> T lock(AbstractZookeeperLock<T> zkLock) {
        String path = zookeeperProperties.getBasePath() + zkLock.getLockPath();
        InterProcessMutex mutex = new InterProcessMutex(this.getClient(), path);
        boolean success = false;
        try {
            try {
                success = mutex.acquire(zkLock.getTimeout(), zkLock.getTimeUnit());
            } catch (Exception e) {
                throw new RuntimeException("obtain lock error " + e.getMessage() + ", path " + path);
            }
            if (success) {
                return (T) zkLock.execute();
            } else {
                return null;
            }
        } finally {
            try {
                if (success){
                    mutex.release();
                }
            } catch (Exception e) {
                log.error("release lock error {}, path {}", e.getMessage(), path);
            }
        }
    }

    public void init() {
        this.client = CuratorFrameworkFactory.builder()
                .connectString(zookeeperProperties.getServer())
                .connectionTimeoutMs(zookeeperProperties.getConnectionTimeout())
                .sessionTimeoutMs(zookeeperProperties.getSessionTimeout())
                .retryPolicy(new ExponentialBackoffRetry(SLEEP_TIME_MS, MAX_RETRIES))
                .build();
        this.client.start();
    }

    public void destroy() {
        try {
            if (getClient() != null) {
                getClient().close();
            }
        } catch (Exception e) {
            log.error("stop zookeeper client error {}", e.getMessage());
        }
    }

}
