package com.yln.data.redis.core;

import com.alibaba.fastjson.support.spring.GenericFastJsonRedisSerializer;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @description key(String)->value(Object)<br/>
 * value序列化采用的是GenericFastJsonRedisSerializer
 *
 * @author: xiaoyulin
 * @create: 2021-05-08 15:32
 **/
public class StringObjectRedisTemplate extends RedisTemplate<String, Object> {

    public StringObjectRedisTemplate() {
        this.setKeySerializer(new StringRedisSerializer());
        this.setHashKeySerializer(new StringRedisSerializer());

        RedisSerializer<Object> fastJsonRedisSerializer = new GenericFastJsonRedisSerializer();
        this.setValueSerializer(fastJsonRedisSerializer);
        this.setHashValueSerializer(fastJsonRedisSerializer);
    }

    public StringObjectRedisTemplate(RedisConnectionFactory connectionFactory) {
        this();
        this.setConnectionFactory(connectionFactory);
        this.afterPropertiesSet();
    }

}
