package com.yln.data.redis.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @description Redis Helper
 * @author: xiaoyulin
 * @create: 2021-05-08 15:37
 **/
public class RedisHelper {

    private final static Logger log = LoggerFactory.getLogger(RedisHelper.class);

    private RedisTemplate redisTemplate;

    public RedisHelper(RedisTemplate redisTemplate){
        this.redisTemplate = redisTemplate;
    }

    /**
     * 设置缓存
     * @param key
     * @param value
     */
    public void set(final String key, final Object value){
        assert (key != null) : "key is null";
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 设置缓存
     * @param key
     * @param value
     * @param expirationSec 过期秒数
     */
    public void set(final String key, final Object value, final int expirationSec){
        assert (key != null) : "key is null";
        redisTemplate.opsForValue().set(key, value, expirationSec, TimeUnit.SECONDS);
    }

    /**
     * set if absent
     * @param key
     * @param value
     * @return
     */
    public boolean setnx(final String key, final Object value) {
        assert (key != null) : "key is null";
        return redisTemplate.opsForValue().setIfAbsent(key, value);
    }

    /**
     * set if absent
     * @param key
     * @param value
     * @param expirationSec 过期秒数
     * @return
     */
    public boolean setnx(final String key, final Object value, final int expirationSec) {
        assert (key != null) : "key is null";
        return redisTemplate.opsForValue().setIfAbsent(key, value, Duration.ofSeconds(expirationSec));
    }

    /**
     * 设置key过期时间
     *
     * @param key
     * @param seconds
     * @return
     */
    public boolean expire(final String key, final int seconds) {
        assert (key != null) : "key is null";
        return redisTemplate.expire(key, seconds, TimeUnit.SECONDS);
    }

    /**
     * 缓存增长
     * @param key
     * @param value
     * @return 增长后的值
     */
    public Long incrBy(String key, Long value){
        assert (key != null) : "key is null";
        assert (value != null) : "value is null";
        return redisTemplate.opsForValue().increment(key, value);
    }

    /**
     * 获取缓存
     * @param key
     * @param <V>
     * @return
     */
    public <V> V get(final String key){
        assert (key != null) : "key is null";
        return (V) redisTemplate.opsForValue().get(key);
    }

    /**
     * 覆盖(持久化)
     * 将给定 key 的值设为 value ，并返回 key 的旧值(old value)
     * 当 key 存在但不是字符串类型时，返回一个错误
     * key 不存在时，返回 null
     *
     * @param key
     * @param value
     * @return
     */
    public <V> V getSet(String key, V value) {
        assert (key != null) : "key is null";
        return (V) redisTemplate.opsForValue().getAndSet(key, value);
    }

    /**
     * 以秒为单位，返回给定 key 的剩余生存时间
     *
     * @param key
     * @return
     */
    public long ttl(String key) {
        assert (key != null) : "key is null";
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 删除缓存
     * @param key
     * @return
     */
    public boolean del(final String key){
        assert (key != null) : "key is null";
        return redisTemplate.delete(key);
    }

    // hash start
    /**
     * 哈希表存值hset key field value<br/>
     *
     * @param key
     * @param field
     * @param value
     * @return
     */
    public void hset(final String key, final String field, final Object value) {
        assert (key != null) : "key is null";
        assert (field != null) : "field is null";
        redisTemplate.opsForHash().put(key, field, value);
    }

    /**
     * hash增长
     * @param key
     * @param field
     * @param delta
     */
    public void hincr(final String key, final String field, final long delta) {
        assert (key != null) : "key is null";
        assert (field != null) : "field is null";
        redisTemplate.opsForHash().increment(key, field, delta);
    }

    /**
     * 哈希表存值hset key field value<br/>
     * set if absent
     *
     * @param key
     * @param field
     * @param value
     * @return
     */
    public boolean hsetnx(final String key, final String field, final Object value) {
        assert (key != null) : "key is null";
        assert (field != null) : "field is null";
        return redisTemplate.opsForHash().putIfAbsent(key, field, value);
    }

    /**
     * 哈希表取值hget key field<br/>
     *
     * @param key
     * @param field
     * @return
     */
    public <V> V hget(final String key, final String field) {
        assert (key != null) : "key is null";
        assert (field != null) : "field is null";
        return (V) redisTemplate.opsForHash().get(key, field);
    }

    /**
     * 获取hashmap
     * @param key
     * @param <V>
     * @return
     */
    public <V> Map<String, V> entries(final String key){
        assert (key != null) : "key is null";
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * hash size
     * @param key
     * @return
     */
    public long hsize(final String key){
        assert (key != null) : "key is null";
        return redisTemplate.opsForHash().size(key);
    }
    // hash end

    // zset start
    /**
     * zset存入
     * @param key
     * @param value
     * @param score
     */
    public void zAdd(String key, Object value, Double score) {
        assert (key != null) : "key is null";
        assert (value != null) : "value is null";
        redisTemplate.opsForZSet().add(key, value, score);
    }

    /**
     * zset加score
     * @param key
     * @param value
     * @param score
     * @return 返回加之后的得分
     */
    public Double zIncr(String key, Object value, Double score) {
        assert (key != null) : "key is null";
        assert (value != null) : "value is null";
        return redisTemplate.opsForZSet().incrementScore(key, value, score);
    }

    /**
     * zset的清空
     *
     * @param key
     * @start 0 开始
     * @end  -1 为最大
     */
    public void zremrange(String key, long start ,long end) {
        assert (key != null) : "key is null";
        redisTemplate.opsForZSet().removeRange(key, start, end);
    }

    /**
     * 获取排行榜(score高->低排序)
     *
     * @param key
     * @param start
     * @param end
     * @return
     */
    public <V> List<ZSetOperations.TypedTuple<V>> zRevRangeWithScores(final String key, final long start, final long end) {
        assert (key != null) : "key is null";
        Set<ZSetOperations.TypedTuple<V>> set = redisTemplate.opsForZSet().reverseRangeWithScores(key, start, end);
        return new ArrayList<>(set);
    }

    /**
     * 获取排行榜(score高->低排序)
     *
     * @param key
     * @param start
     * @param end
     * @return
     */
    public <V> List<V> zRevRange(final String key, final long start, final long end) {
        assert (key != null) : "key is null";
        Set<V> set = redisTemplate.opsForZSet().reverseRange(key, start, end);
        return new ArrayList<>(set);
    }

    /**
     * 获取排名(score高->低排序)
     * @param key
     * @param value
     * @return
     */
    public long zrevrank(String key, Object value){
        assert (key != null) : "key is null";
        assert (value != null) : "value is null";
        return redisTemplate.opsForZSet().reverseRank(key, value);
    }

    /**
     * 获取得分
     * @param key
     * @param value
     * @return
     */
    public Double zScore(String key, Object value){
        assert (key != null) : "key is null";
        assert (value != null) : "value is null";
        return redisTemplate.opsForZSet().score(key, value);
    }

    /**
     * 获取sort list(score低->高排序)
     *
     * @param key
     * @param start
     * @param end
     * @return
     */
    public <V> List<ZSetOperations.TypedTuple<V>> zRangeWithScores(final String key, final long start, final long end) {
        assert (key != null) : "key is null";
        Set<ZSetOperations.TypedTuple<V>> set = redisTemplate.opsForZSet().rangeWithScores(key, start, end);
        return new ArrayList<>(set);
    }

    /**
     * 获取sort list(score低->高排序)
     *
     * @param key
     * @param start
     * @param end
     * @return
     */
    public <V> List<V> zRange(final String key, final long start, final long end) {
        assert (key != null) : "key is null";
        Set<V> set = redisTemplate.opsForZSet().range(key, start, end);
        return new ArrayList<>(set);
    }

    /**
     * 删除指定values
     * @param key
     * @param values
     * @return
     */
    public long zremove(final String key, final Object... values){
        assert (key != null) : "key is null";
        return redisTemplate.opsForZSet().remove(key, values);
    }
    // zset end

    // list start
    /**
     * 获取list长度
     * @param key
     * @return
     */
    public Long lLen(final String key){
        assert (key != null) : "key is null";
        return redisTemplate.opsForList().size(key);
    }

    /**
     * list left push
     * @param key
     * @param value
     * @return 返回list长度
     */
    public Long lpush(final String key, final Object value){
        assert (key != null) : "key is null";
        assert (value != null) : "value is null";
        return redisTemplate.opsForList().leftPush(key, value);
    }

    /**
     * list right pop
     * @param key
     * @param <V>
     * @return
     */
    public <V> V rpop(final String key){
        assert (key != null) : "key is null";
        return (V) redisTemplate.opsForList().rightPop(key);
    }

    /**
     * list right pop
     * @param key
     * @param timeoutSec 超时时间（单位s）
     * @param <V>
     * @return
     */
    public <V> V rpop(final String key, long timeoutSec){
        return (V) redisTemplate.opsForList().rightPop(key, timeoutSec, TimeUnit.SECONDS);
    }

    /**
     * 取list某区间
     * @param key
     * @param start
     * @param end
     * @param <V>
     * @return
     */
    public <V> List<V> lRange(final String key, final long start, final long end){
        assert (key != null) : "key is null";
        return redisTemplate.opsForList().range(key, start, end);
    }

    /**
     * 获取某一位置下对象
     * @param key
     * @param index
     * @param <V>
     * @return
     */
    public <V> V lIndex(final String key, long index){
        assert (key != null) : "key is null";
        return (V) redisTemplate.opsForList().index(key, index);
    }
    // list end

    // set start
    /**
     * set add
     * @param key
     * @param values
     * @return
     */
    public long sadd(final String key, Object... values) {
        return redisTemplate.opsForSet().add(key, values);
    }

    /**
     * 获取set集合
     * @param key
     * @param <V>
     * @return
     */
    public <V> Set<V> members(final String key) {
        return redisTemplate.opsForSet().members(key);
    }

    /**
     * 是否存在
     * @param key
     * @param value
     * @return
     */
    public boolean isMember(final String key, Object value) {
        return redisTemplate.opsForSet().isMember(key, value);
    }
    // set end

    // lock start
    /**
     * Redis分布式锁
     *
     * @param lockKey
     * @param lockValue  为了便于加锁客户端取消自己的锁，建议这里使用UUID
     * @param lockSec    value有效时间
     * @param timeoutSec 线程等待最大时间
     * @return
     */
    public boolean tryLock(String lockKey, String lockValue, int lockSec, int timeoutSec) {
        if (redisTemplate == null) {
            return false;
        }
        // 当前时间毫秒值
        long start = System.currentTimeMillis();
        for(;;){
            // 获取锁
            boolean locked = this.setnx(lockKey, lockValue, lockSec);
            if (locked) {
                return true;
            } else {
                // 当前时间的毫秒值
                long now = System.currentTimeMillis();
                // 当前线程等待的时间
                long costed = now - start;
                // 如果等待时间超过过期时间，则返回false
                if (costed >= timeoutSec * 1000) {
                    log.info("RedisLock tryLock :" + costed + ", LockKey: " + lockKey + ", LockValue: " + lockValue);
                    return false;
                }
            }
        }
    }

    /**
     * 线程主动释放锁，而不是等到锁失效
     *
     * @param lockKey
     * @param lockValue
     */
    public void releaseLock(String lockKey, String lockValue) {
        if (lockKey == null || lockValue == null) {
            return;
        }
        // 判断加锁与解锁是不是同一个客户端
        if (lockValue.equals(this.get(lockKey))) {
            // 若在此时，这把锁突然不是这个客户端的，则会误解锁
            this.del(lockKey);
        }
    }
    // lock end

}
