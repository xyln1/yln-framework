package com.yln.data.redis.autoconfigure;

import com.yln.data.redis.core.StringObjectRedisTemplate;
import com.yln.data.redis.helper.RedisHelper;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.integration.redis.util.RedisLockRegistry;

import java.net.UnknownHostException;

/**
 * @description
 * @author: xiaoyulin
 * @create: 2021-05-08 13:52
 **/
@Configuration
@AutoConfigureAfter(RedisAutoConfiguration.class)
public class MyRedisAutoConfiguration {

    @ConditionalOnMissingBean(StringObjectRedisTemplate.class)
    @Bean
    public StringObjectRedisTemplate stringObjectRedisTemplate(RedisConnectionFactory redisConnectionFactory) throws UnknownHostException {
        StringObjectRedisTemplate template = new StringObjectRedisTemplate(redisConnectionFactory);
        return template;
    }

    @Bean
    public RedisLockRegistry redisLockRegistry(RedisConnectionFactory redisConnectionFactory) {
        return new RedisLockRegistry(redisConnectionFactory, "REDIS_LOCK");
    }

    @Bean
    public RedisHelper redisHelper(StringObjectRedisTemplate redisTemplate){
        return new RedisHelper(redisTemplate);
    }

}
