# yln-starter-dubbo

#### 介绍
Dubbo spring boot开箱组件

#### 使用说明
启用：@EnableDubbo 
1、server端配置：

    spring.application.name=user-service
    #如果指定了spring应用名称，可以缺省dubbo的应用名称，这2个至少要配置1个。缺省dubbo的应用名称时默认值是spring的应用名称
    #dubbo.application.name=user-service
    
    #注册中心地址
    dubbo.registry.address=zookeeper://127.0.0.1:2181
    #端口号可以写在address中，也可以单独写。实质是从address中获取的port是null，后面设置的port覆盖了null
    #dubbo.registry.port=2181
    
    #指定dubbo使用的协议、端口
    dubbo.protocol.name=dubbo
    dubbo.protocol.port=20880
    
    #指定注册到zk上超时时间，ms
    dubbo.registry.timeout=10000
    
    #指定实现服务(提供服务）的包
    dubbo.scan.base-packages=com.xxx.provider
    
2、client端配置：
   
    #应用名称
    spring.application.name=order-service
    #dubbo.application.name=order-service
   
    #注册中心地址
    dubbo.registry.address=zookeeper://192.168.1.9:2181
    #dubbo.registry.port=2181
   
    #协议、端口
    dubbo.protocol.name=dubbo
    dubbo.protocol.port=20880
   
    #连接zk的超时时间，ms
    dubbo.registry.timeout=10000
   
    #启动应用时是否检查注册中心上有没有依赖的服务，默认true
    #dubbo.consumer.check=false
